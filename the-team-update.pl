#!/usr/bin/perl -w

# be strict with us ;)
use strict;
use warnings;

# generate the team page as markdown
print "Generating the-team.md page...\n";
my $the_team_md = "content/the-team.md";
open (my $the_team, ">$the_team_md");
print $the_team <<'TEAM_HEADER_EOF'
---
title: The Team
author: Christoph Cullmann
date: 2010-07-09T09:03:57+00:00
---

## Who are the Kate & KWrite contributors?

[Kate & KWrite](https://invent.kde.org/utilities/kate) and the underlying [KTextEditor](https://invent.kde.org/frameworks/ktexteditor) & [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting) frameworks are created by a group of volunteers around the world.
The same is true for [this website](https://invent.kde.org/websites/kate-editor-org).

Below is a periodically updated list of the contributors to the Git repositories containing the above named software components & website.
As this list is purely based on the Git history of our repositories, it will be incomplete for otherwise submitted patches, etc.

The list is sorted by number of commits done by the individual contributors.
This is by no means the best measure of the impact of their contributions, but gives some rough estimation about their level of involvement.

## Don't forget the KDE Community!

Beside these explicitly named contributors to our text editor related components, a large portion of work is done by other members of the much broader [KDE community](https://kde.org/).

This includes crucial work like:

* developing the foundations we use, like KDE Frameworks
* doing all internationalization and translation work for our projects
* releasing our stuff
* maintaining our infrastructure (GitLab, CI, ...)
* supporting developer sprints

If you are not interested in [joining our text editor related team](/join-us/), please take a look if you want to [contribute to KDE](https://community.kde.org/Get_Involved).
The KDE community is a very welcoming bunch of people.

If you are not able to contribute, you might be interested to [donate](https://kde.org/donations).
Whereas these donations are not directly targeted at specific development, they help to keep the overall KDE community going.
For example some Kate related coding sprints were funded by the [KDE e.V.](https://ev.kde.org) based on these donations.

TEAM_HEADER_EOF
;

# the repos we are interested in
my %repos = (
    "." => ".",
    "kate" => "git\@invent.kde.org:utilities/kate.git",
    "ktexteditor" => "git\@invent.kde.org:frameworks/ktexteditor.git",
    "syntax-highlighting" => "git\@invent.kde.org:frameworks/syntax-highlighting.git",
);

# get the repos
for my $repo (sort keys %repos) {
    # clone from invent.kde.org, update if already around, if not . aka this website!
    if ($repo ne ".") {
        if (-d $repo) {
            print "Updating $repo clone...\n";
            system("git", "-C", $repo, "pull", "-q") == 0 || die "Failed to pull $repo!\n";
        } else {
            print "Creating $repo clone...\n";
            system("git", "clone", "-q", $repos{$repo}) == 0 || die "Failed to clone $repo from $repos{$repo}!\n";
        }
    }
}

# mapping of some contributor names, either to a canonical one or the void
my %contributorMapping = (
    # ignore some accounts like scripts
    "Script Kiddy" => "",
    "l10n daemon script" => "",
    "KDE Sysadmin" => "",

    # canonical names for some people
    "jonathan poelen" => "Jonathan Poelen",
    "mentasti" => "Marco Mentasti",
);

# function to compute contributors (all or since some time)
sub computeContributors
{
    my $contributors = shift;
    my $limit = shift;
    if (!defined($limit)) {
        $limit = "";
    }

    # get stats per repo
    for my $repo (sort keys %repos) {
        # get short stats with commits => names
        for (`git -C $repo shortlog $limit -s -n`) {
            if ((my $commits, my $contributor) = /\s*([0-9]+)\s+(.*)/) {
                # remap, if needed, might map to empty string for triggering of filter below
                if (defined($contributorMapping{$contributor})) {
                    $contributor = $contributorMapping{$contributor};
                }

                # skip bad contributors
                next if ($contributor =~ /^\s*$/);

                # remember number of commits, accumulated over all repositories
                if (!defined($contributors->{$contributor})) {
                    $contributors->{$contributor} = 0;
                }
                $contributors->{$contributor} += int($commits);
            }
        }
        if ($? != 0) {
            die "Failed to retrieve contributors for $repo!\n";
        }
    }

    # use "SPDX-FileCopyrightText:" for stuff not tracked by version control
    # only do this, if no date limit given
    if ($limit eq "") {
        for my $repo (sort keys %repos) {
            for (`git -C $repo grep "SPDX-FileCopyrightText:"`) {
                if ((my $contributor) = /SPDX-FileCopyrightText:\s+(?:[0-9]+)\s+(.*)\s+</) {
                    # remap, if needed, might map to empty string for triggering of filter below
                    if (defined($contributorMapping{$contributor})) {
                        $contributor = $contributorMapping{$contributor};
                    }

                    # skip bad contributors
                    next if ($contributor =~ /^\s*$/);

                    # we have here no commits, we just add people that are not known by commits (with one dummy commit)
                    if (!defined($contributors->{$contributor})) {
                        $contributors->{$contributor} = 1;
                    }
                }
            }
            if ($? != 0) {
                die "Failed to retrieve contributors for $repo via SPDX-FileCopyrightText grep!\n";
            }
        }
    }
}

# helper to print contributors for given contributors hash
sub printContributors
{
    my $contributors = shift;
    my $oldContributors = shift;

    # we now have all contributors
    my $entry = 0;
    print $the_team "<table>\n";
    for my $contributor (sort { int($contributors->{$b}) <=> int($contributors->{$a}) } sort keys %{$contributors}) {
        if ($entry % 2 == 0) {
            if ($entry > 0) {
                print $the_team "</tr>\n";
            }
            print $the_team "<tr>\n";
        }

        # highlight new contributors?
        if (defined($oldContributors) && !defined($oldContributors->{$contributor})) {
            print $the_team "<td><b>$contributor</b> <!-- $contributors->{$contributor} --></td>\n";
        } else {
            print $the_team "<td>$contributor <!-- $contributors->{$contributor} --></td>\n";
        }

        ++$entry;
    }
    print $the_team "</table>\n\n";
}

# fill contributor of last year => commits map
my %contributorsOfLastYear = ();
computeContributors(\%contributorsOfLastYear, "--since=1.years");

# fill contributor before last year => commits map
my %contributorsBeforeLastYear = ();
computeContributors(\%contributorsBeforeLastYear, "--until=1.years");

# fill total contributor => commits map
my %contributors = ();
computeContributors(\%contributors);

# contributors of the last year
print $the_team "## Contributors during the last year\n";
print $the_team "<b>".scalar(keys %contributorsOfLastYear)." people</b> contributed during the last year.\n";
print $the_team "New contributors are highlighted, thanks for joining our team!\n\n";
printContributors(\%contributorsOfLastYear, \%contributorsBeforeLastYear);

# all contributors ever
print $the_team "## Contributors during the project lifetime\n";
print $the_team "During the full project lifetime <b>".scalar(keys %contributors)." people</b> contributed.\n";
print $the_team "Thanks for making Kate possible!\n\n";
printContributors(\%contributors);

# we are done with the page => ensure close before git add happens
close($the_team);
