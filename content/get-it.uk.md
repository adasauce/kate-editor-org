---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: "\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 Kate"
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux та UNIX-и

* Встановіть [Kate](https://apps.kde.org/en/kate) або [KWrite](https://apps.kde.org/en/kwrite) зі [сховищ пакунків для вашого дистрибутива](https://kde.org/distributions/)

{{< get-it-button-discover

button-kate-title="Встановлення Kate "button-kate-subtitle="за допомогою Discover" button-kate-alt="Встановлення Kate за допомогою Discover або інших крамниць програм AppStream"

button-kwrite-title="Встановити KWrite" button-kwrite-subtitle="за допомогою Discover" button-kwrite-alt="Встановити KWrite за допомогою Discover або інших крамниць програм AppStream"

>}}

Ці кнопки працюють лише у [Discover](https://apps.kde.org/en/discover) та інших крамницях програм AppStream, зокрема [Програмах GNOME](https://wiki.gnome.org/Apps/Software). Ви також можете скористатися програмою для керування пакунками вашого дистрибутива, зокрема [Muon](https://apps.kde.org/en/muon) або [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

* [Пакунок Flatpak Kate у Flathub](https://flathub.org/apps/details/org.kde.kate)

{{< get-it-button link="https://flathub.org/apps/details/org.kde.kate" img="/images/flathub-badge-i-en.svg" width="200" alt="Download Kate on Flathub" />}}

* [Пакунок Snap Kate у Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-white.svg" width="200" alt="Get Kate on the Snap store" />}}

* [Випуск Kate (64-бітовий) у форматі пакунка AppImage](https://binary-factory.kde.org/job/Kate_Release_appimage/) *

* [Щоденна збірка Kate (64-бітова) у форматі пакунка AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **

* [Зберіть програму](/build-it/#linux) з початкових кодів.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate у Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.svg" width="200" alt="Get Kate from the Microsoft store" />}}

* [Kate у Chocolatey](https://chocolatey.org/packages/kate)

* [Засіб встановлення випусків Kate (64-бітовий)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *

* [Засіб встановлення щоденної версії Kate (64-бітовий)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **

* [Зберіть програму](/build-it/#windows) з початкових кодів.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

* [Засіб встановлення випуску Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *

* [Засіб встановлення щоденної версії Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **

* [Зберіть програму](/build-it/#mac) з початкових кодів.

{{< /get-it >}}



{{< get-it-info >}}

**Про випуски:** <br>[Kate](https://apps.kde.org/en/kate) і [KWrite](https://apps.kde.org/en/kwrite) є частинами [збірки програм KDE](https://apps.kde.org), яку [типово випускають 3 рази на рік](https://community.kde.org/Schedules). [Рушій редагування тексту](/about-katepart/), [теми кольорів](/themes/) та [підсвічування синтаксису](/syntax/) є частинами бібліотек [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), які [оновлюють щомісяця](https://community.kde.org/Schedules/Frameworks). Про нові випуски оголошують [тут](https://kde.org/announcements/).

\* У цих пакунках **випуску** містяться дані найсвіжішої версії Kate і KDE Frameworks.

\*\* **Щоденні** пакунки автоматично збираються з початкового коду щодня, тому вони можуть бути нестабільними у роботі і містити вади або можливості, які реалізовано не повністю. Рекомендуємо користуватися ними лише з метою тестування.