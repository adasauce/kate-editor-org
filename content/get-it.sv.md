---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: "H\xE4mta Kate"
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux och Unix

* Installera [Kate](https://apps.kde.org/en/kate) eller [Kwrite](https://apps.kde.org/en/kwrite) från din [distribution](https://kde.org/distributions/)

{{< get-it-button-discover

button-kate-title="Installera Kate" button-kate-subtitle="via Discover "button-kate-alt="Installera Kate via Discover eller annan AppStream programbutik"

button-kwrite-title="Installera Kwrite" button-kwrite-subtitle="via Discover" button-kwrite-alt="Installera Kwrite via Discover eller annan AppStream programbutik"

>}}

Knapparna fungerar bara med [Discover](https://apps.kde.org/en/discover) och andra AppStream programbutiker, såsom [GNOME programvara](https://wiki.gnome.org/Apps/Software). Det går också att använda en distributions pakethanterare, såsom [Muon](https://apps.kde.org/en/muon) eller [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

* [Kates Flatpak-paket på Flathub](https://flathub.org/apps/details/org.kde.kate)

{{< get-it-button link="https://flathub.org/apps/details/org.kde.kate" img="/images/flathub-badge-i-en.svg" width="200" alt="Download Kate on Flathub" />}}

* [Kates Snap-paket på Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-white.svg" width="200" alt="Get Kate on the Snap store" />}}

* [Kates 64-bitars utgåva Appimage-paket](https://binary-factory.kde.org/job/Kate_Release_appimage/) *

* [Kates nattliga 64-bitars AppImage-paket](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **

* [Bygg det](/build-it/#linux) från källkod.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate på Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.svg" width="200" alt="Get Kate from the Microsoft store" />}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Kates (64-bitars) installationsverktyg för utgåvor](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *

* [Kates nattliga (64-bitars) installationsverktyg](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **

* [Bygg det](/build-it/#windows) från källkod.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

* [Kates installationsverktyg för utgåvor](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *

* [Kates nattliga installationsverktyg](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **

* [Bygg det](/build-it/#mac) från källkod.

{{< /get-it >}}



{{< get-it-info >}}

**Om utgåvorna:** <br>[Kate](https://apps.kde.org/en/kate) och [Kwrite](https://apps.kde.org/en/kwrite) ingår i [KDE Program](https://apps.kde.org), som normalt [ges ut tillsammans tre gånger om året](https://community.kde.org/Schedules). [Textredigeringsgränssnittet](/about-katepart/), [färgteman](/themes/) och [syntaxfärgläggningen](/syntax/) tillhandahålls av [KDE Ramverk](https://kde.org/announcements/kde-frameworks-5.0/), som [uppdateras månadsvis](https://community.kde.org/Schedules/Frameworks). Nya utgåvor tillkännages [här](https://kde.org/announcements/).

\* Paketen benämnda **utgåvor** innehåller senaste versionen av Kate och KDE Ramverk.

\*\* De **nattliga** paketen kompileras automatiskt från källkod, och kan därför vara instabila och innehålla fel eller ofullständiga funktioner. De rekommenderas bara i utprovningssyfte.