---
author: Christoph Cullmann
date: 2010-07-09 08:40:19
title: "Fonctionnalit\xE9s"
---
## Fonctionnalités de l'application

 ! [Copie d'écran de Kate, affichant la fonctionnalité de partage de fenêtres et le module externe de terminal](/images/kate-window.png)

* Afficher et modifier de multiples documents en même temps, en séparant horizontalement et verticalement la fenêtre.

* Beaucoup de modules externes : [Terminal intégré ](https://konsole.kde.org), module externe « SQL », module externe de compilation, module externe « GDB », remplacement dans les fichiers et bien plus

* Interface multi-documents (MDI)

* Prise en charge de session

## Fonctionnalités générales

 ! [Copie d'écran de Kate, affichant la fonctionnalité de recherche et de remplacement](/images/kate-search-replace.png)

* Prise en charge de l'encodage (Unicode et bien d'autres)

* Prise en charge du rendu de texte bi-directionnel

* Prise en charge de la fin de ligne (Windows, Unix, Mac), y compris la détection automatique

* Transparence réseau (ouverture de fichiers distants)

* Extensible grâce à des scripts

## Fonctionnalités avancées de l'éditeur

 ! [Copie d'écran de la bordure Kate avec les numéros de ligne et les signets](/images/kate-border.png)

* Système de signets (aussi pris en charge : points d'arrêt, etc.)

* Marques de barres de défilement

* Indicateurs de modifications de lignes

* Numéros de lignes

* Repliage de code

## Coloration syntaxique

 ! [Copie d'écran de Kate pour la fonctionnalité de coloration syntaxique](/images/kate-syntax.png)

* Prise en charge de la coloration syntaxique de plus de 300 langages

* Correspondance des parenthèses

* Vérification orthographique intelligente et à la volée

* Mise en valeur des mots sélectionnés

## Fonctionnalités de programmation

 ! [Copie d'écran de Kate pour la fonctionnalité de programmation](/images/kate-programming.png)

* Indentation automatique programmable

* Gestion intelligente pour commenter et dé-commenter

* Auto-complémentation avec les suggestions d'arguments

* Mode de saisie « vi »

* Mode de sélection avec bloc rectangulaire

## Chercher et remplacer

 ! [Copie d'écran de Kate pour la fonctionnalité de recherche incrémentale](/images/kate-search.png)

* Recherche incrémentale, connue aussi comme &#8220;trouver comme vous saisissez&#8221;

* Prise en charge de la recherche et le remplacement multi-lignes

* Prise en charge des expressions rationnelles

* Rechercher et remplacer dans de multiples fichiers ouverts ou dans des fichiers sur le disque

## Sauvegarder et restaurer

 ! [Copie d'écran de Kate pour la fonctionnalité de récupération de données après plantage](/images/kate-crash.png)

* Archivage à la sauvegarde

* Échanger les fichiers pour retrouver les données après un plantage du système

* Système de « Annuler » / « Refaire »