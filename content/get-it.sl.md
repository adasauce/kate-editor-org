---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Pridobi Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux in unixi

* Namesti [Kate](https://apps.kde.org/en/kate) ali [KWrite](https://apps.kde.org/en/kwrite) iz [vaše distribucije](https://kde.org/distributions/)

{{< get-it-button-discover

button-kate-title="Namesti Kate" button-kate-subtitle="prek Discover"button-kate-alt="Namesti Kate prek Discover-ja ali ostalih skladišč aplikacij"

button-kwrite-title="Namesti KWrite" button-kwrite-subtitle="prej Discover-ja"button-kwrite-alt="Namesti KWrite prek Discover-ja ali drugih skladišč aplikacij"

>}}

Ti gumbi delujejo samo s programi [Discover](https://apps.kde.org/en/discover) in drugimi skladišči aplikacij kot so [GNOME Software](https://wiki.gnome.org/Apps/Software). Lahko uporabite tudi namestilnik programskih paketov vaše distribucije kot npr. [Muon](https://apps.kde.org/en/muon) ali [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

* [Kate's Flatpak package v Flathub](https://flathub.org/apps/details/org.kde.kate)

{{< get-it-button link="https://flathub.org/apps/details/org.kde.kate" img="/images/flathub-badge-i-en.svg" width="200" alt="Download Kate on Flathub" />}}

* [Kate's Snap package v Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-white.svg" width="200" alt="Get Kate on the Snap store" />}}

* [Kate's izdaja (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Release_appimage/) *

* [Kate's nočni (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **

* [Build it](/build-it/#linux) iz vira.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate v trgovini Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.svg" width="200" alt="Get Kate from the Microsoft store" />}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Kate izdaja (64bit) namestilnik](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *

* [Kate nočni (64bit) namestilnik](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **

* [Izgradi ga](/build-it/#windows) iz vira.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate release installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *

+ [Kate nightly installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **

+ [Build it](/build-it/#mac) from source.

{{< /get-it >}}



{{< get-it-info >}}

**O izdajah:** <br>[Kate](https://apps.kde.org/en/kate) in [KWrite](https://apps.kde.org/en/kwrite) sta dela [KDE Applications](https://apps.kde.org), ki so [praviloma masovno izdane 3-krat na leto](https://community.kde.org/Schedules). [Koda urejevalnika besedil](/about-katepart/), [barvnih tem](/themes/) in  [osvetljevanje skladnje](/syntax/) so zagotovljene v [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), ki so [mesečno posodobljene](https://community.kde.org/Schedules/Frameworks). Nove izdaje so objavljene [tukaj](https://kde.org/announcements/).

\*  Paketi **ob izdaji** vsebujejo zadnjo verzijo programa Kate in KDE Frameworks.

\*\* The **nightly** packages are automatically compiled daily from source code, therefore they may be unstable and contain bugs or incomplete features. These are only recommended for testing purposes.