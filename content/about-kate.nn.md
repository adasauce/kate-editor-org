---
author: Christoph Cullmann
date: 2010-07-09 08:40:19
title: Funksjonar
---
## Programfunksjonar

![Skjermbilete av Kate med delte vindauge og eit terminaltillegg](/images/kate-window.png)

* Vis og rediger fleire dokument samtidig ved å dela vindauge vass- eller loddrett

* Mange programtillegg – [innebygd terminal](https://konsole.kde.org), SQL, program­kompilering, GDB, tekstutbyting i filer, med meir

* Fleirdokument-grensesnitt (MDI)

* Støtte for økter

## Generelle funksjonar

![Skjermbilete som viser «søk og byt ut»-funksjonen i Kate](/images/kate-search-replace.png)

* Støtte for ulike teiknkodingar (Unicode og mange andre)

* Støtte for tekstvising både frå venstre mot høgre og omvend

* Støtte for ulike linjeskift-typar (Windows, Unix og Mac), med automatisk oppdaging av rett type

* Nettverks­transparens (kan opna filer på eksterne maskiner/tenester)

* Kan utvidast via skripting

## Avanserte redigerings­funksjonar

![Skjermbilete av dokument­margen i Kate, med linjenummer og bokmerke](/images/kate-border.png)

* Bokmerke­system (støttar òg avbrotspunkt og liknande)

* Merke i rullefeltet

* Merke for linjeendringar

* Linjenummer

* Kodefalding

## Syntaksmerking

![Skjermbilete av Kate med syntaksmerking](/images/kate-syntax.png)

* Syntaksmerking for over 300 språk

* Markering av parentespar

* Smart automatisk stavekontroll

* Markering av førekomstar av merkt tekst i resten av dokumentet

## Programmerings­funksjonar

![Skjermbilete av programmerings­funksjonar i Kate](/images/kate-programming.png)

* Skriptbare automatiske innrykk

* Smart kommentering og avkommentering

* Autofullføring med argumenthint

* Vi-tastemodus

* Blokkmerkingsmodus

## Funksjonalitet for søk og tekstutbyting

![Skjermbilete av inkrementelt søk i Kate](/images/kate-search.png)

* Inkrementelt søk, òg kjend som «find as you type»

* Støtte for søk og utbyting av tekst over fleire linjer

* Støtte for regulære uttrykk

* Samtidig søk og utbyting av tekst i fleire opna filer eller filer på disken

## Reservekopiar og gjenoppretting

![Skjermbilete av Kate ved gjenoppretting etter programkrasj](/images/kate-crash.png)

* Automatiske reservekopiar ved lagring

* Vekslefiler for å gjenoppretta data ved systemkrasj

* Støtte for angring/omgjering