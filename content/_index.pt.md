---
layout: index
---
O Kate é um componente de edição multi-documentos que faz parte do [KDE](https://kde.org) desde a versão 2.2. Sendo uma das [aplicações do KDE](https://kde.org/applications), o Kate traz a transparência na rede, assim como a integração com as funcionalidades espantosas do KDE. Escolha-o para ver o código-fonte em HTML no Konqueror, editar ficheiros de configuração, criar novas aplicações ou qualquer outra tarefa de edição de texto. Só irá necessitar de apenas uma instância em execução do Kate. [Saiba mais...](/about/)

![Imagem do Kate a apresentar vários documentos e o emulador do terminal numa
compilação](/images/kate-window.png)