---
title: Syntax Highlighting Checker
author: Dominik Haumann

date: 2017-12-03T17:20:47+00:00
url: /2017/12/03/syntax-highlighting-checker/
categories:
  - Common
  - Developers
  - Users
tags:
  - planet

---
The KTextEditor Framework uses the syntax highlighting files provided by the KSyntaxHighlighting Framework since the  [KDE Frameworks release 5.28][1].

The KSyntaxHighlighting Framework implements Kate&#8217;s highlighting system and meanwhile is used in quite some applications (e.g. LabPlot, KDE PIM). What is quite nice is that the KSyntaxHighlighting framework is nicely unit tested. And while we do not have tests for all highlighting files, we still provide some quality assurance through a compile time checker.

How does it work? Well &#8211; in former times, Kate loaded all highlighting .xml files from disk (through the KTextEditor framework). This lead to a slow startup over time, since there are >250 .xml files that needed a stat system call at startup.

With the KSyntaxHighlighting Framework, all these xml files are compiled into a Qt resource (qrc file), that then is included into the KSyntaxHighlighting library.

In order to create the Qt resource file, we need to iterate over all available xml files anyways. So what happens is that we take this opportunity and also scan the highlighting files for common mistakes.

As of today, we are checking the following:

  1. RegExpr: A warning is raised, if a regular expression has syntax errors.
  2. DetectChars: A warning is raised, if the char=&#8221;x&#8221; attribute contains more or less than one character, e.g. when char=&#8221;xyz&#8221;, or char=&#8221;\\&#8221; (no escaping required), or similar.
  3. Detect2Chars: Same as DetectChars, just for char=&#8221;x&#8221; and char1=&#8221;y&#8221;.
  4. Keyword lists: A warning is raised, if a keyword entry contains leading or trailing spaces. Additional trimming just takes time.
  5. Keyword lists: A warning is raised if a keyword list is unused.
  6. Keyword lists: A warning is raised if multiple keyword lists use the same me (=identifier).
  7. Keyword lists: A warning is raised if a non-existing keyword list is used.
  8. Contexts: A warning is raised, if a non-existing context is referenced.
  9. Contexts: A warning is raised, if a context is unused.
 10. Contexts: A warning is raised, if multiple contexts have the same name (identifier clash).
 11. Attributes: A warning is raised, if non-existing itemData is used.
 12. Attributes: A warning is raised, if multiple itemDatas use the same name (identifier clash).
 13. Attributes: A warning is raised, if an itemData is unused.

This list helps us nicely to catch many mistakes at compile time even before running unit tests.

**Update (2017-12-17):** All above issues are fixed for all highlighting files starting with the KSyntaxHighlighting 5.42 framework, to be released in January 2018.

 [1]: /2016/11/15/ksyntaxhighlighting-a-new-syntax-highlighting-framework/