---
title: Porting KTextEditor to KSyntaxHighlighting – Folding
author: Christoph Cullmann

date: 2018-08-04T15:37:44+00:00
url: /2018/08/04/porting-ktexteditor-to-ksyntaxhighlighting-folding/
categories:
  - Common
  - Developers
  - KDE

---
After fixing some first porting bugs to KSyntaxHighlighting, code folding (non-indentation based) is back working, too.

There is a still a lot to do (and e.g. the syntax colors are still kind of randomized), but already all KTextEditor original highlighting code is gone without ending up in an unusable state.

<img class="aligncenter size-full wp-image-4159" src="/wp-content/uploads/2018/08/kwrite_folding.png" alt="" width="778" height="727" srcset="/wp-content/uploads/2018/08/kwrite_folding.png 778w, /wp-content/uploads/2018/08/kwrite_folding-300x280.png 300w, /wp-content/uploads/2018/08/kwrite_folding-768x718.png 768w" sizes="(max-width: 778px) 100vw, 778px" />