---
title: Hugo Extended on CentOS 7
author: Christoph Cullmann

date: 2019-04-06T20:55:00+00:00
excerpt: |
  After first using the Hermit theme, I moved to the Hugo Coder theme to have a nicer front page and menu at the top.
  Unfortunately that needs Hugo in the &ldquo;extended&rdquo; version.
  The binary one can download on https://github.com/gohugoio/hugo/rel...
url: /posts/hugo-extended-centos-7/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/hugo-extended-centos-7/
syndication_item_hash:
  - f8c108fab5b6e38ee2f38a0d533f93e1
  - 5ff9b6443ae30a2995ddd92a979773e5
  - 9f30c49166ca5f816b979474e2f8505e
  - c84e4b4b939649940bf4f96f97ce0a3d
categories:
  - Common

---
After first using the Hermit theme, I moved to the Hugo Coder theme to have a nicer front page and menu at the top. Unfortunately that needs Hugo in the &ldquo;extended&rdquo; version. The binary one can download on https://github.com/gohugoio/hugo/releases doesn&rsquo;t run on CentOS 7, unlike the normal variant you get there (too new libstdc++ needed). One has to recompile it to get that working. To make it easier for others, to do so, just: