---
title: 'Kate: What’s cool, and what should be improved?'
author: Dominik Haumann

date: 2014-03-18T10:59:06+00:00
url: /2014/03/18/kate-whats-cool-and-what-should-be-improved/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
This is sort of a poll: Please write in the comments below exactly

  * **one line about what you like** on Kate, and
  * **one line what you want improved.**

Please spread the word so we get a lot of feedback &#8211; Thanks! :-)