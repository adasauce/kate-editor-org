---
title: 'Kate’s Mascot: Kate the Woodpecker'
author: Christoph Cullmann

date: 2014-10-12T12:47:54+00:00
url: /2014/10/12/kates-mascot-kate-the-woodpecker/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
After the first KF 5 release, I contacted the creator of the Krita mascot Kiki and the KF 5  dragons artwork, Tyson Tan, if he would be interested in design a Kate mascot, too. He immediately agreed to help out and after some months of roundtrips, here we go!

**Kate has a mascot: _Kate the Woodpecker_**

The short design summary (by Tyson Tan):

> Why a woodpecker? I said I was going to draw a humming bird, but she turned out to look more like a woodpecker, and the sound a woodpecker makes knocking on a tree is coincidentally similar to the sound of keyboard strokes).
> 
> Kate is female because of her name. I thought about other names like Klicky and Katherine, but I would rather save them for other future KDE projects to pick up as their names.
> 
> Design elements:  
> &#8220;{}&#8221; on the chest, &#8220;/&#8221; of the feathers, and &#8220;*&#8221; in the eyes.  
> The wing feathers also resembles multiple document tabs.  
> Color scheme inspired by doxygen source code highlighting.

And how does the first version of the mascot look like? Here is the mandatory version 1.0 mascot picture:

[<img class="aligncenter size-large wp-image-3429" src="/wp-content/uploads/2014/10/mascot_20140930_kate_fin-853x1024.png" alt="Kate Mascot: Kate the Woodpecker" width="474" height="569" srcset="/wp-content/uploads/2014/10/mascot_20140930_kate_fin-853x1024.png 853w, /wp-content/uploads/2014/10/mascot_20140930_kate_fin-249x300.png 249w" sizes="(max-width: 474px) 100vw, 474px" />][1]

Tyson Tan allows that artwork to be  Creative Commons BY-SA and/or GPL and/or GFDL licensed and donates it to the KDE Kate project.

This is version 1.0, changes might be made and more variants are possible in the future.

Thanks to Tyson Tan for this contribution, he rocks (more of his artwork can be found on his <a title="Tyson Tan" href="http://tysontan.com/" target="_blank">homepage</a>).  We will see the mascot soon a lot more on the Kate homepage and other Kate material.

This once more shows: the community shall never underestimate any non-code contributions. You designers, translators, documentation writers, &#8230;., that help us all out, you all rock!

 [1]: /wp-content/uploads/2014/10/mascot_20140930_kate_fin.png