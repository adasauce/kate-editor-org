---
title: GSoC – Swap Files for Kate
author: dianat

date: 2010-07-12T09:59:04+00:00
url: /2010/07/12/gsoc-swap-files-for-kate/
categories:
  - Developers
tags:
  - planet

---
Hello,

As mid-term evaluations have started, I would like to show my current state of GSoC project, because I&#8217;ve never found the time to do it.

The swap file feature is implemented, except for the view differences feature and few TODOs. Some more testing need to be done, though. Below are some screenshots of how it works.

When you start editing in a document, a swap file for the document is created (&#8220;.swp.originalFileName&#8221;). If Kate crashes and the user didn&#8217;t save the changes, the swap file remains on the disk.

[<img class="aligncenter size-large wp-image-394" src="/wp-content/uploads/2010/07/snapshot4-1024x640.png" alt="" width="600" height="375" srcset="/wp-content/uploads/2010/07/snapshot4-1024x640.png 1024w, /wp-content/uploads/2010/07/snapshot4-300x187.png 300w, /wp-content/uploads/2010/07/snapshot4.png 1440w" sizes="(max-width: 600px) 100vw, 600px" />][1]

On the load of the original file, Kate checks if there is a swap file on the disk, and if it is, a warning bar pops from the top, asking if you either want to recover the data loss, discard it or view differences. If the recover button is pressed, the lost data is replayed. Otherwise, if the discard button is pressed, the swap file is removed from the disk. This also happens on normal close and on save of the original file.

[<img class="aligncenter size-large wp-image-395" src="/wp-content/uploads/2010/07/snapshot5-1024x640.png" alt="" width="600" height="375" srcset="/wp-content/uploads/2010/07/snapshot5-1024x640.png 1024w, /wp-content/uploads/2010/07/snapshot5-300x187.png 300w, /wp-content/uploads/2010/07/snapshot5.png 1440w" sizes="(max-width: 600px) 100vw, 600px" />][2]

<div>
  <div>
    Many thanks to Christoph and Dominik who helped me get it through.
  </div>
</div>

 [1]: /wp-content/uploads/2010/07/snapshot4.png
 [2]: /wp-content/uploads/2010/07/snapshot5.png