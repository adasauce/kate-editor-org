---
title: Text Folding Reloaded
author: Christoph Cullmann

date: 2013-03-24T15:54:59+00:00
url: /2013/03/24/text-folding-reloaded/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
If we look at the incoming bug reports, a lot still seem to be located in our code folding implementation.

The problem of the current code folding code, even after the GSoC rewrite of large parts, is that it is inter weaved with the syntax highlighting engine.

During highlighting, we try to build up some tree containing all folding start/ends. To accommodate languages that have indentation based folding like Python, we started to fake such starts/ends which are normally tokens like { or }. As these tree can be unbalanced and contain overlapping regions, the implementation is quiet complex.

Additionally, as the folding relies heavily on the syntax highlighting, which is done per document, it is impossible to have different folding per view.

After looking again at the code to solve some issues, it got more and more clear to me, that we need to restart that from scratch.

Instead of feeding the highlighting info into a tree, I started to implement text folding that just allows you to mark regions as folding ranges manually. Then internally a non-overlapping tree of ranges is created that uses our moving cursors to keep track of changes in the document. Each view will have an instance of such a tree, which allows to have folding per view.

These folds are only created on demand, e.g. if you decide to fold some region via selection + &#8220;fold&#8221; command. This already works quiet well in my Kate repository clone. The missing part is now the integration with the highlighting, that should use this well defined API to only create a folding region in the moment the user wants to fold it. In addition some API for the highlighters is needed to indicate that for some line there is the fold action. After that is done, I might consider to replace the current implementation with the novel one, which is btw. much easier to unit test (which is already done for most API primitives).

Btw., the work is done in <a title="Kate Code Folding Repository" href="http://quickgit.kde.org/?p=clones%2Fkate%2Fcullmann%2Fkate-codefolding.git" target="_blank">this</a> repository, if somebody wants to contribute, patches are welcome .P