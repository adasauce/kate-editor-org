---
title: Video Tutorials Learning C++ (in German)
author: Dominik Haumann

date: 2013-10-23T13:39:43+00:00
url: /2013/10/23/video-tutorials-learning-c-in-german/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
I just stumbled over a really good <a title="Learning C++ (German)" href="http://www.youtube.com/watch?v=nFM0SuPSxnc&list=PLfthkO_8dYCqARBKnZQm6Ck614Td9Vmz7" target="_blank">tutorial on youtube</a> that teaches C++ from the very beginning (in German). The tutorial is split into lots of small episodes, each about 10 to 15 minutes. The quality of these tutorials is very good in terms of video, voice and also contents. Subject is mostly &#8220;pure&#8221; C++ and later a bit of Qt is used, so it does not cover C nor lots of additional libraries. Still, if you want to understand the details, you might want to give it a try :-)

PS: The tutorials use Kate Part, just look at the code folding bar on the left. So <a title="Author" href="http://bytesnobjects.dev.geekbetrieb.de/kontakt" target="_blank">the author</a> is definitely doing something right :-p