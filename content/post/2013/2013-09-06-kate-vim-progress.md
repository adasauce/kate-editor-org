---
title: Kate Vim Progress
author: Simon St James

date: 2013-09-06T15:13:50+00:00
url: /2013/09/06/kate-vim-progress/
pw_single_layout:
  - "1"
categories:
  - Common
tags:
  - planet

---
Hi all,

Just an eye-glazingly brief blog post about some of the stuff I&#8217;ve been working on in the Kate Vim emulation mode since my last [blog][1]. Once more, I&#8217;ll mostly be dumping them all as bullet points, Gräßlin-Style™, with some longer sections on the more important changes, but this time around I&#8217;ve added some nifty animated gifs which you can skip to should the full text prove too harrowing!

In roughly chronological order, I:

  * Fixed regression with gv (&#8220;re-select last selection&#8221;).
  * Laid the groundwork for improved behaviour when repeating a change that involved an auto-completion, for now just fixing a [bug][2] where if auto-completion was automatically invoked and we repeat the change via &#8220;.&#8221;, only a portion of what we typed gets repeated.
  * As promised, implemented &#8220;yank highlighting&#8221;, which shows the portion of the text that you just yanked; very handy if you do complex yanks like e.g &#8220;y2f)&#8221;:

[<img class="aligncenter" src="/wp-content/uploads/2013/09/kateyankhighlight-static.png" alt="" />][3]

<center>
  <em>&#8220;Yank Highlighting&#8221;, 444KB, Click to Play.</em>
</center>&nbsp;

  * Fixed a [bug][4] with keypad handling.
  * Made ctrl-p and ctrl-n wrap around in completion lists.

&#8230; and a biggie:

## Emulated Vim Command Bar

There are quite a lot of issues with the interaction between Vim mode and Kate&#8217;s own Find dialog:

  * The cursor is not positioned at the beginning of the match.
  * The full match is not highlighted.
  * &#8220;?&#8221; does not search backwards(!)
  * The Find dialog cannot be used in mappings.
  * The Find dialog could not be used to either record or playback macros.
  * Aborts Visual Mode when summoned.

etc. On top of that, we miss out on some nice features of Vim&#8217;s search bars, such as ctrl-r for insertion of register contents; ctrl-w for deleting previous word; ctrl-h for delete; ctrl-b/e for jumping to the beginning or end (more conveniently located, IMO, than HOME or END); plus the Qt regex syntax is different from Vim&#8217;s. In light of this, I decided to add a new KateViewBarWidget called &#8220;KateViEmulatedCommandBar&#8221;:

[<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimsearch-static.png" alt="" />][5]

<center>
  <em>&#8220;Emulated Command Bar Search&#8221;, 697KB, Click to Play.</em>
</center>This behaves much more like Vim, and has the features and bugfixes mentioned above; it even does a reasonable job (in current master; it missed 4.11) of translating Vim regexes to Qt ones e.g. with Vim, a &#8220;+&#8221; is a literal &#8220;+&#8221;, not an &#8220;at least one of the preceding&#8221; marker; with Qt it is the reverse. The emulated command bar performs this, and other, conversions for you.

The searches carried out can be repeated correctly via &#8220;n&#8221; and &#8220;N&#8221;. Search history can be navigated via ctrl-p and ctrl-n, and searches &#8220;external&#8221; to the emulated command bar (e.g. those carried out by &#8220;*&#8221; and &#8220;#&#8221;) are added to this history. &#8220;Smart Case&#8221; is used for specifying case-sensitivity &#8211; if the search term has any capitals, then the search is assumed to be case-sensitive; else it is case-insensitive. More on the emulated command bar later! Back to the micro-fixes:

  * Distinguished recursive vs non-recursive mappings; inspired by this [bug report][6] which highlighted a nasty crashing problem if we tried to map j -> gj and k -> gk &#8230;
  * &#8230; and relatedly, completely reworked gk and gj, fixing a bunch of bugs in the process.
  * Yet more fixes to the &#8220;inner block&#8221; text object, which is much more complex than you would credit!
  * A bunch of fixes and patches to ctrl-a/ctrl-x (&#8220;increment/ decrement number under-or-to-the-right-of cursor&#8221;) culminating in me just writing the whole thing from scratch :)
  * Indented paste (]p, ]P etc).

Everything above made it for 4.11, I think. The Emulated Command Bar was disabled by default but can be enabled by setting the hidden config option &#8220;Vi Input Mode Emulate Command Bar&#8221; to &#8220;true&#8221;. Continuing:

  * Made &#8220;/&#8221; and &#8220;?&#8221;, when using the Emulated Command Bar, usable as (countable) motions: so one can, for example, do d3/foo<enter> to delete up to just before the third occurence of &#8220;foo&#8221;.
  * Allowed one to set the whole selection to a given text object even after we&#8217;ve formed a selection in Visual Mode e.g. you can start visual mode, navigate to inside a pair of brackets, and do &#8220;ib&#8221; to re-set the current selection to just the inside of that pair of brackets. I think this worked once upon a time and I broke it, so this is more of a regression fix than a new feature :/
  * Made the emulated command bar usable for Commands (like the &#8220;F7&#8221; command box in normal Kate) as well as for forwards and backwards searches when summoned via &#8220;:&#8221;. Note that since the emulated command bar is usable in mappings, we can now incorporate the execution of your Javascript script(s) into the Vim key mapping system, which could end up being a very powerful addition.
  * Allowed mappings for InsertMode and VisualMode as well as for NormalMode. Oddly, this was accomplished mainly as a side-effect of a fix for a tiny bug with mappings. There is no gui for this, yet, but I&#8217;ve implemented imap, vmap, inoremap, etc so they can be set from the command line. I&#8217;ve also added the <space> special key for use in mappings, so that you can do e.g. imap a<space>b c<space>d to specify a mapping containing spaces from the command line.
  * Implemented &#8220;g~&#8221; (&#8220;toggle case&#8221; with motion).
  * A bunch more fixes for t/T/f/F to bring them more in line with Vim&#8217;s behaviour, especially when there aren&#8217;t enough characters to find.
  * Made &#8216;r&#8217; (&#8220;replace single character&#8221;) counted, so it can actually replace several characters.
  * Made &#8220;\C&#8221; (&#8220;case-sensitivity marker&#8221;) work in the emulated command-bar search: this marker is used to specify that the current search should be case-sensitive (handy if you want a case-sensitive search for an all-lower-case search term) after which it is stripped out and ignored completely.
  * Made &#8220;//&#8221; and &#8220;??&#8221; (&#8220;repeat last search forward/ backwards&#8221;) work in the emulated command bar, and also allow the &#8220;/e&#8221; marker (&#8220;place the cursor at the end of the match&#8221;), although this latter is a little buggy at the moment.
  * Added fix for the usage of Alt-0, Alt-1 etc shortcuts in normal mode; this is for use with KDevelop&#8217;s automatic refactoring support which relies on these shorcuts (e.g. &#8220;Rename [variable] to [newvariable] Alt-0 | Hide Alt-1&#8221;, etc).
  * A bunch of fixes for J (&#8220;join lines&#8221;), including making it work in Visual Mode when the selection runs from top to bottom; not crashing if all lines are empty; fixing countedness (it would join one too many lines); cursor positioning after the join; etc.
  * Fixed occasional bug where counted inserts e.g. 5ixyz<esc> would start each repetition on a new line.
  * Implemented iW and aW (&#8220;inner/ a WORD&#8221;) text objects.

&#8230; and then on to:

## Making the Emulated Command Bar Suitable for Default

The emulated command bar has a lot of advantages over the default Find dialog and command-line for Kate Vim users, but there are drawbacks, too: firstly, it can only perform regex searches and not literal searches, which is awkward when searching for complex expressions pasted from your document that may contain characters that have a special meaning when interpreted as regex&#8217;s (e.g. &#8220;*&#8221;) or which have special meaning in a Vim search bar (e.g. &#8220;/&#8221; when searching forward, and &#8220;?&#8221; when searching backwards). The necessity of going through the search term and escaping all such characters is a big strike against its use.

Secondly, Vim&#8217;s Find/ Replace dialog has most of the same flaws, when used in Vim mode, as its Find dialog has: in fact, incremental Find & Replace doesn&#8217;t [work][7] at all with Vim mode(!), and the emulated command bar is of no use here.

Since I&#8217;d really like to make the emulated command bar the default (and possibly only) option for use in Vim mode, I really needed to sort out a solution for these two dealbreakers.

For the first, there are basically two options: one is to add some kind of flag that says &#8220;look for literal matches for this string; don&#8217;t treat it as a regex!&#8221;; the other is to make it easy to perform the escaping of special characters. I eventually opted for the second, as it allowed one to treat portions of an expression as a regex if one desired, plus it allowed one to always use the same delimiters in a sed-replace expression (i.e. :s/find/replace/gci) without having to worry about whether the &#8220;find&#8221; term contained &#8220;/&#8221;&#8216;s or not. So I added a shortcut (ctrl-g) to the emulated command bar that behaved much like ctrl-r (&#8220;insert contents of the following register&#8221;) except that it escapes the inserted register contents in such a way that a _regex_ search for the contents would behave the same as a _literal_ search for the contents. The gif below highlights the problem and the solution; the &#8220;:) :) :)&#8221; bit used ctrl-g to insert the (correctly escaped) yanked expression:  
[<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimctrl-g-static.png" alt="" />][8]

<center>
  <em>&#8220;Escape Expression for Regex&#8221;, 483KB, Click to Play.</em>
</center>For the second problem, improving the good old sed-replace seemed the obvious choice, so I added some code in the emulated command bar to translate the regex in the &#8220;find&#8221; term from Vim style to Qt style. Kate&#8217;s own Find/ Replace dialog maintains separate histories for both the &#8220;find&#8221; and &#8220;replace&#8221; terms, so I needed to follow suit if I wanted the emulated command bar to be a credible replacement: if the cursor is placed over the &#8220;find&#8221; or &#8220;replace&#8221; terms in a sed-replace expression, pressing ctrl-p will summon the relevant history:

[<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimsedreplacehistory-static.png" alt="" />][9]

<center>
  <em>&#8220;Sed Search/ Replace History&#8221;, 259KB, Click to Play.</em>
</center>Kate&#8217;s own Find/ Replace dialog makes it easy to focus on the &#8220;find&#8221; and &#8220;replace&#8221; fields, so I also added shortcuts &#8220;ctrl-d&#8221; and &#8220;ctrl-f&#8221; to the emulated command bar that clears and places the cursor at the &#8220;find&#8221; and &#8220;replace&#8221; portions of a sed-replace expression, respectively. As an extra bonus, ctrl-space can be used anywhere in the emulated command bar to complete words from the document, which is handy when filling out your find/ replace terms.

Still missing, though, were interactive search and replace; multi-line search; usage of \U and \L in the &#8220;replace&#8221; expression to make regex captures upper/ lower cased; etc. Multi-line search and \U and \L codes are available in a class called KateRegExpSearch, so I initially pulled out the guts of the SedReplace code and replaced it with that, and then made the whole thing interactive:

[<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimmultilinesedreplace-static.png" alt="" />][10]

<center>
  <em>&#8220;Sed Multi-Line&#8221;, 399KB, Click to Play.</em>
</center>(in the gif above, I constructed my regex with the standard &#8220;/&#8221; search bar so I could get immediate feedback as I am a giant regex baby who still needs his stabilisers; this final regex was then added to the &#8220;find&#8221; history, which I then added to the sed-replace search expression with a ctrl-d + ctrl-p).

The emulated command bar now seemed to me to be a decent alternative to Kate&#8217;s own Find/ Find & Replace dialogs, so with that out of the way, I then moved on to &#8230;

## Macro Support!

The lack of macro support is what got me into Kate Vim hacking in the first place, so it seems weird that it ended up being almost the last thing I tackled. The reason, of course, is that macro support would be a bit crippled if we couldn&#8217;t run searches or execute commands in our macros, so I really needed to finish the emulated command bar first. It ended up being quite easy in the end; the main difficulty was in interacting with mappings, especially the situation where a mapping triggered a macro which in turn needed to execute another mapping! Anyway, here&#8217;s a small, real-life example of a Kate Vim macro in action:

[<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimmacro-static.png" alt="" />][11]

<center>
  <em>&#8220;Macro&#8221;, 300KB, Click to Play.</em>
</center>One difficulty with macros comes from recording and replaying auto-completions. In Vim itself, macros are &#8220;dumb&#8221; sequence of keypresses that are recorded and replayed verbatim, including those that summon, navigate and execute lists of completions. In Kate Vim, though, this approach doesn&#8217;t work so well, partly because the available completions at any time are less predictable (e.g. we could have a function appear in the list of completions when we record the macro, but not when we replay it, due to e.g. problems with the C++ parser etc) and also because, in KDevelop at least, the added completions are context-sensitive: for example, a completion of a function will add its own opening bracket if there is not one immediately after the cursor position, but will re-use the existing one otherwise. The &#8220;repeat last change&#8221; runs into a similar problem and has some measures to mitigate this, but is still far from fool-proof. The gif below illustrates the problem:

[<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimcompletion-old-static.png" alt="" />][12]

<center>
  <em>&#8220;Old Completion&#8221;, 184KB, Click to Play.</em>
</center>In what probably represents the worst effort:payoff ratio in all of history, I spent a while re-working the macro recording and playback system to store attributes of completions as they occur, and then attempt to play them back using the same context-sensitive system; the &#8220;repeat last change&#8221; mechanism was re-worked to also use this system, so I&#8217;ll use that to demonstrate:

[<img class="aligncenter" src="/wp-content/uploads/2013/09/katevimcompletion-new-static.png" alt="" />][13]

<center>
  <em>&#8220;New Completion&#8221;, 176KB, Click to Play.</em>
</center>&nbsp;

That&#8217;s about it for macro support; now back to the small changes!

  * Fixed regression where opening a view and immediately giving it a selection (for example, by clicking an entry in KDevelop&#8217;s grepview) didn&#8217;t switch to Visual mode, leaving us with a &#8220;stuck&#8221; selection that we needed to manually enter & exit Visual Mode to clear.
  * Got rid of the baffling &#8220;No more chars for bookmark&#8221; message every time we set a breakpoint in KDevelop ;)
  * Fixed crashes with guu & gUU on empty line.
  * Implemented command-mode mappings (cmap etc) for use in the emulated command bar; this was prompted by my preference for using &#8220;ctrl-e&#8221; instead of &#8220;ctrl-g&#8221; for the &#8220;insert escaped register contents&#8221; (&#8216;e&#8217; is right next to &#8216;r&#8217; on my keyboard) and for &#8220;ctrl-m&#8221; instead of &#8220;ctrl-e&#8221; (&#8220;jump to the end of the command bar&#8221;).
  * Implemented the *unmap (vunmap, iunmap, cunmap etc) family of commands for removing mappings from the command line.

&#8230; and that&#8217;s about it! I&#8217;m going to take a break from Kate stuff for a while to work on some other projects I&#8217;ve been neglecting, but will return to finish off some odds and ends later :)

The Kate Vim mode is in pretty good shape nowadays, so if you were thinking of trying it out, now (with the current master) would be a good time :)

 [1]: /2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff
 [2]: https://bugs.kde.org/show_bug.cgi?id=311831
 [3]: /wp-content/uploads/2013/09/kateyankhighlight.gif
 [4]: https://bugs.kde.org/show_bug.cgi?id=293120
 [5]: /wp-content/uploads/2013/09/katevimsearch.gif
 [6]: https://bugs.kde.org/show_bug.cgi?id=314415
 [7]: https://bugs.kde.org/show_bug.cgi?id=182508
 [8]: /wp-content/uploads/2013/09/katevimctrl-g.gif
 [9]: /wp-content/uploads/2013/09/katevimsedreplacehistory.gif
 [10]: /wp-content/uploads/2013/09/katevimmultilinesedreplace.gif
 [11]: /wp-content/uploads/2013/09/katevimmacro.gif
 [12]: /wp-content/uploads/2013/09/katevimcompletion-old.gif
 [13]: /wp-content/uploads/2013/09/katevimcompletion-new.gif