---
title: New Mac OS X Build Documentation
author: Christoph Cullmann

date: 2016-06-14T21:52:50+00:00
url: /2016/06/14/new-mac-os-x-build-documentation/
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
During the Mac OS X platform meeting here at Randa, the point was made that the current build documentation for Mac OS X is a bit lacking, as e.g. the steps to create an application bundle are only documented somewhere for Kate, Krita, KDevelop, &#8230; but in no central place.

I started to writeup a [new howto][1] at the [mac.kde.org][2] page that utilizes emerge for the heavy lifting like the KDE Windows team uses it.

I think this might be nice to have synergy effects between both ports, thanks to Hannah for hinting to this and the help for setting it up. Might be interesting for the CI, too.

[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][3]

 [1]: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Mac
 [2]: https://community.kde.org/Mac#Building_KDE_Frameworks_based_software_on_Mac_OS_X
 [3]: https://www.kde.org/fundraisers/randameetings2016/