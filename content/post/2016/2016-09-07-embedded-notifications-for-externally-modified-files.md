---
title: Embedded Notifications for Externally Modified Files
author: Dominik Haumann

date: 2016-09-07T20:51:33+00:00
url: /2016/09/07/embedded-notifications-for-externally-modified-files/
categories:
  - Developers
  - Events
  - Users
tags:
  - planet

---
In the past, KTextEditor notified the user about externally modified files with a modal dialog. Many users were annoyed by this behavior.

Starting with KDE Frameworks 5.27, KTextEditor will show an embedded widget for external changes, see:

[<img class="aligncenter size-full wp-image-3963" src="/wp-content/uploads/2016/09/changed-notification.png" alt="Change Notifications" width="890" height="491" srcset="/wp-content/uploads/2016/09/changed-notification.png 890w, /wp-content/uploads/2016/09/changed-notification-300x166.png 300w, /wp-content/uploads/2016/09/changed-notification-768x424.png 768w, /wp-content/uploads/2016/09/changed-notification-672x372.png 672w" sizes="(max-width: 890px) 100vw, 890px" />][1]

 [1]: /wp-content/uploads/2016/09/changed-notification.png