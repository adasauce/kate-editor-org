---
title: GSoC – Kate Code Folding (Bug-less)
author: Adrian Lungu

date: 2011-08-07T20:21:28+00:00
url: /2011/08/07/gsoc-–-kate-code-folding-bug-less/
categories:
  - Developers
  - Users

---
Hi everyone!

Another season of Google Summer of Code is approaching its end. :(

I feel pretty bad about that, because I enjoy it and I love working on Kate.

I used the last two weeks to solve all the bugs and requests (folding related) from [bugs.kde.org][1]. If you [find][2] any folded related bugs, please send the report using [bugs.kde.org][1] and I will solve them (the warranty period is unlimited, so you don’t have to worry about that ;) )

The next two weeks (the time remained until the dead line) will be used to write the documentation and to do some look changes, according to the code reviews received from the Kate community. Also, I will write some tests. Writing automatic tests for the folding is not such an easy task because you have to check a lot of stuff: the highlight, the folding sign, the folded lines, the line that remains visible and other things. And there are so many languages that act differently. That’s why I preferred to do the tests manually, so far. Anyway, I will write some units that will test the bases of the code folding, like number of lines left after a folding or some delete/insert operations occurred.

I consider this project a success and I hope that the Kate team has no regrets for choosing me for this GSoC season. :)

Enjoy Kate’s new code folding! ;)

Adrian

 [1]: https://bugs.kde.org/
 [2]: /get-it/