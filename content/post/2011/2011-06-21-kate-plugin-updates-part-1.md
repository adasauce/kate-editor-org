---
title: Kate plugin updates part 1
author: Kåre Särs

date: 2011-06-21T03:32:24+00:00
url: /2011/06/21/kate-plugin-updates-part-1/
categories:
  - Developers
  - Users
tags:
  - planet

---
<div>
  KDE SC (or was it Platform?) 4.7 will bring some enhancements to Kate plugins. First up is the build plugin. The new thing for the build plugin is the ability to specify multiple targets per session. This means that you can build debug/releas/&#8230; targets without having to jump between sessions. The default setup/targets contain a configure target with a CMake command and a build target that runs make.
</div>

<div>
  <p>
    <figure id="attachment_937" aria-describedby="caption-attachment-937" style="width: 719px" class="wp-caption alignnone"><a href="/wp-content/uploads/2011/06/Config.png"><img class="size-full wp-image-937" src="/wp-content/uploads/2011/06/Config.png" alt="" width="719" height="197" srcset="/wp-content/uploads/2011/06/Config.png 719w, /wp-content/uploads/2011/06/Config-300x82.png 300w" sizes="(max-width: 719px) 100vw, 719px" /></a><figcaption id="caption-attachment-937" class="wp-caption-text">Screenshot of the default "Config" target</figcaption></figure>
  </p>
  
  <p>
    <figure id="attachment_939" aria-describedby="caption-attachment-939" style="width: 716px" class="wp-caption alignnone"><a href="/wp-content/uploads/2011/06/Build1.png"><img class="size-full wp-image-939" src="/wp-content/uploads/2011/06/Build1.png" alt="" width="716" height="193" srcset="/wp-content/uploads/2011/06/Build1.png 716w, /wp-content/uploads/2011/06/Build1-300x80.png 300w" sizes="(max-width: 716px) 100vw, 716px" /></a><figcaption id="caption-attachment-939" class="wp-caption-text">Screenshot of the default "Build" target</figcaption></figure>
  </p>
  
  <p>
    Targets can be added, copied and deleted. If the last target is deleted the default targets will be re-added.
  </p>
</div>

<div>
  When the targets are set up, you can of course select a specific target by selecting the settings tab and there select the desired target from the small combo box. To make target switching faster, there is a &#8220;Next Target&#8221; action. This action will switch to the next target in the target combo-box. If the settings tab is not visible it will open the tab to display the current target. There is no default shortcut-key for the actions, but fortunately it is not hard to change a shortcut in KDE applications :) My personal shortcut-setup can be seen in the build-menu screen-shot.
</div>

<div>
  <p>
    <figure id="attachment_940" aria-describedby="caption-attachment-940" style="width: 250px" class="wp-caption alignnone"><a href="/wp-content/uploads/2011/06/Menu.png"><img class="size-full wp-image-940" src="/wp-content/uploads/2011/06/Menu.png" alt="" width="250" height="227" /></a><figcaption id="caption-attachment-940" class="wp-caption-text">Screenshot of the "Build" menu</figcaption></figure>
  </p>
  
  <p>
    Next up will be the GDB plugin
  </p>
</div>