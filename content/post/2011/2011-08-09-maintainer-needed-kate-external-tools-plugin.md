---
title: 'Maintainer Needed: Kate – External Tools Plugin'
author: Christoph Cullmann

date: 2011-08-09T16:11:49+00:00
url: /2011/08/09/maintainer-needed-kate-external-tools-plugin/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
The &#8220;External Tools&#8221; plugin is one of the oldest plugins of Kate. A big hug to Anders Lund for writing it years ago, a lot of people enjoyed using it.

<p style="text-align: center;">
  <a href="/wp-content/uploads/2011/08/externaltools.png"><img class="alignnone size-full wp-image-1108" title="External Tools Configuration Dialog" src="/wp-content/uploads/2011/08/externaltools.png" alt="" width="774" height="444" srcset="/wp-content/uploads/2011/08/externaltools.png 774w, /wp-content/uploads/2011/08/externaltools-300x172.png 300w" sizes="(max-width: 774px) 100vw, 774px" /></a>
</p>

It allows the user to specify own commands (aka snippets of shell programs) to be executed with the text of the current document or the current file.

Basic stuff you can do with it:

  * Create you a little command that shows an Git/SVN/&#8230; diff of current file
  * Sort the file with command line sort program (or more advanced scripts)
  * Pipe the text of the document into your most loved perl script

Unfortunately there is no maintainer for it at the moment. Therefore it will be disabled for KDE 4.8, as a lot of bugs sneaked into it during the late 3.x and whole 4.x life-cycle. (Mostly because of bad porting done by me and others and no time to fixup any real bugs)

If you want to step up for maintainer ship, please contact me (cullmann@kde.org) or kwrite-devel@kde.org.

Thanks a lot in advance ;P

Btw., some of the bugs (now no longer valid but interesting for future maintainers):

  * https://bugs.kde.org/show_bug.cgi?id=117946
  * https://bugs.kde.org/show_bug.cgi?id=181528
  * https://bugs.kde.org/show_bug.cgi?id=261210
  * https://bugs.kde.org/show_bug.cgi?id=97569
  * https://bugs.kde.org/show_bug.cgi?id=110485
  * https://bugs.kde.org/show_bug.cgi?id=153963