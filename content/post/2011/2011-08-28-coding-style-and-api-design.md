---
title: Coding Style and API Design
author: Dominik Haumann

date: 2011-08-28T19:06:45+00:00
url: /2011/08/28/coding-style-and-api-design/
pw_single_layout:
  - "1"
categories:
  - Developers
  - KDE
tags:
  - planet

---
Once in a while, there are really interesting blogs, presentations and reports about how to do things right as software developer. This is a quick list of links about API design and coding style:

  1. <a title="Designing Qt-Style C++ APIs" href="http://doc.qt.nokia.com/qq/qq13-apis.html" target="_blank">Designing Qt-Style C++ APIs (Qt4)</a>, by Matthias Ettrich, 2005
  2. <a title="API Design Principles" href="http://developer.qt.nokia.com/wiki/API_Design_Principles" target="_blank">API Design Principles</a>, extended version of (1), Qt Developer Network (Nokia)
  3. <a title="The Little Manual of API Design" href="http://chaos.troll.no/~shausman/api-design/api-design.pdf" target="_blank">The Little Manual of API Design</a>, an in-depth summary of (1) and (2), Jasmin Blanchette, 2008 (Nokia)
  4. <a title="Developing Quality Libraries" href="http://people.canonical.com/~jriddell/gcds-presentations-2009/Developing%20Quality%20Libraries%20Olivier%20Goffart.pdf" target="_blank">Developing Quality Libraries</a>, presentation of (1)-(3) on the Desktop Summit in Gran Canaria, Oliver Goffart, 2009
  5. <a title="How to Design a Good API and Why it Matters" href="http://www.youtube.com/watch?v=aAb7hSCtvGw" target="_blank">How to Design a Good API and Why It Matters</a>, Joshua Bloch, 2007 (Google)
  6. <a title="Qt Coding Style" href="http://developer.qt.nokia.com/wiki/Qt_Coding_Style" target="_blank">Qt Coding Style</a>, good coding style not limited to Qt, Qt Developer Network (Nokia)
  7. <a title="Guidelines for KDE Development" href="http://techbase.kde.org/Policies" target="_blank">Guidelines for KDE Development</a>, similar to e.g. (6), KDE Techbase
  8. <a title="Hall of API Shame: Boolean Trap" href="http://ariya.ofilabs.com/2011/08/hall-of-api-shame-boolean-trap.html" target="_blank">Hall of API Shame: Boolean Trap</a>, Ariya Hidayat, 2011

Thanks to all the authors for sharing & happy reading!