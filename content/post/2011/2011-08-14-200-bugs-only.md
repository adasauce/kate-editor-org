---
title: 200 Bugs only ;)
author: Christoph Cullmann

date: 2011-08-14T12:34:35+00:00
url: /2011/08/14/200-bugs-only/
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
Kate (App + Part + KWrite) is down to 200 bugs, thanks a lot to all people helping with this effort, e.g. Dominik, Erlend, Adrian, &#8230;

Perhaps others want to join, we have still a lot bugs and many are quiet some work (and stuff like BiDi errors are not that nice to fix, if you have actually no clue about the used languages :/)