---
title: 'Kate: Shortcomings of .kateconfig file'
author: Dominik Haumann

date: 2006-07-23T19:02:00+00:00
url: /2006/07/23/kate-shortcomings-of-kateconfig-file/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/07/kate-shortcomings-of-kateconfig-file.html
categories:
  - Common

---
Assume you have a .kateconfig file optimized for C++ code that replaces tabs on save. Now you open a Makefile that contains tabs (due to its strict syntax). If you save the Makefile in kate, the tabs then are replaced which results in a corrupted Makefile.

In other words: The .kateconfig file applies to <span style="font-style: italic;">every</span> file. <span style="font-weight: bold;">It lacks mimetype/extension support and thus can lead to unwanted behaviours.</span>

So <span style="font-style: italic;">if</span> you use a .kateconfig file, <span style="font-style: italic;">keep</span> that in mind :) Maybe we should simply use modelines in the h/cpp files for now.