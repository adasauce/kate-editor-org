---
title: The Team
author: Christoph Cullmann
date: 2010-07-09T09:03:57+00:00
---

## Who are the Kate & KWrite contributors?

[Kate & KWrite](https://invent.kde.org/utilities/kate) and the underlying [KTextEditor](https://invent.kde.org/frameworks/ktexteditor) & [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting) frameworks are created by a group of volunteers around the world.
The same is true for [this website](https://invent.kde.org/websites/kate-editor-org).

Below is a periodically updated list of the contributors to the Git repositories containing the above named software components & website.
As this list is purely based on the Git history of our repositories, it will be incomplete for otherwise submitted patches, etc.

The list is sorted by number of commits done by the individual contributors.
This is by no means the best measure of the impact of their contributions, but gives some rough estimation about their level of involvement.

## Don't forget the KDE Community!

Beside these explicitly named contributors to our text editor related components, a large portion of work is done by other members of the much broader [KDE community](https://kde.org/).

This includes crucial work like:

* developing the foundations we use, like KDE Frameworks
* doing all internationalization and translation work for our projects
* releasing our stuff
* maintaining our infrastructure (GitLab, CI, ...)
* supporting developer sprints

If you are not interested in [joining our text editor related team](/join-us/), please take a look if you want to [contribute to KDE](https://community.kde.org/Get_Involved).
The KDE community is a very welcoming bunch of people.

If you are not able to contribute, you might be interested to [donate](https://kde.org/donations).
Whereas these donations are not directly targeted at specific development, they help to keep the overall KDE community going.
For example some Kate related coding sprints were funded by the [KDE e.V.](https://ev.kde.org) based on these donations.

## Contributors during the last year
<b>86 people</b> contributed during the last year.
New contributors are highlighted, thanks for joining our team!

<table>
<tr>
<td>Christoph Cullmann <!-- 584 --></td>
<td><b>Waqar Ahmed</b> <!-- 166 --></td>
</tr>
<tr>
<td>Jonathan Poelen <!-- 94 --></td>
<td>Nibaldo González <!-- 71 --></td>
</tr>
<tr>
<td>Kåre Särs <!-- 52 --></td>
<td><b>Jan Paul Batrina</b> <!-- 49 --></td>
</tr>
<tr>
<td>Friedrich W. H. Kossebau <!-- 43 --></td>
<td>Mark Nauwelaerts <!-- 33 --></td>
</tr>
<tr>
<td>Laurent Montel <!-- 32 --></td>
<td>Ahmad Samir <!-- 29 --></td>
</tr>
<tr>
<td>Dominik Haumann <!-- 28 --></td>
<td><b>Alexander Lohnau</b> <!-- 20 --></td>
</tr>
<tr>
<td>Alex Turbov <!-- 17 --></td>
<td>Christoph Feck <!-- 13 --></td>
</tr>
<tr>
<td>Yuri Chornoivan <!-- 12 --></td>
<td>Albert Astals Cid <!-- 9 --></td>
</tr>
<tr>
<td>David Faure <!-- 9 --></td>
<td>Andreas Cord-Landwehr <!-- 8 --></td>
</tr>
<tr>
<td>Nicolas Fella <!-- 8 --></td>
<td>Tomaz Canabrava <!-- 8 --></td>
</tr>
<tr>
<td>Milian Wolff <!-- 7 --></td>
<td>Pino Toscano <!-- 7 --></td>
</tr>
<tr>
<td><b>Andrzej Dabrowski</b> <!-- 6 --></td>
<td>Carl Schwan <!-- 6 --></td>
</tr>
<tr>
<td><b>Samuel Gaist</b> <!-- 6 --></td>
<td>Aleix Pol <!-- 4 --></td>
</tr>
<tr>
<td><b>Xaver Hugl</b> <!-- 4 --></td>
<td><b>Andreas Gratzer</b> <!-- 3 --></td>
</tr>
<tr>
<td>Charles Vejnar <!-- 3 --></td>
<td><b>Ernesto Castellotti</b> <!-- 3 --></td>
</tr>
<tr>
<td><b>Francis Laniel</b> <!-- 3 --></td>
<td>Thomas Friedrichsmeier <!-- 3 --></td>
</tr>
<tr>
<td><b>Arek Koz (Arusekk)</b> <!-- 2 --></td>
<td><b>David Bryant</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Fabian Vogt</b> <!-- 2 --></td>
<td><b>George Florea Bănuș</b> <!-- 2 --></td>
</tr>
<tr>
<td>Luigi Toscano <!-- 2 --></td>
<td><b>Marcello Massaro</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Markus Slopianka</b> <!-- 2 --></td>
<td>Martin Walch <!-- 2 --></td>
</tr>
<tr>
<td>Nate Graham <!-- 2 --></td>
<td>Sven Brauch <!-- 2 --></td>
</tr>
<tr>
<td><b>Vincenzo Buttazzo</b> <!-- 2 --></td>
<td><b>Wes H</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Alex Hermann</b> <!-- 1 --></td>
<td>Alexander Potashev <!-- 1 --></td>
</tr>
<tr>
<td><b>Alexander Schlarb</b> <!-- 1 --></td>
<td>Andreas Hartmetz <!-- 1 --></td>
</tr>
<tr>
<td><b>Andrey Karepin</b> <!-- 1 --></td>
<td>Anthony Fieroni <!-- 1 --></td>
</tr>
<tr>
<td><b>Antoni Bella Pérez</b> <!-- 1 --></td>
<td><b>Ayushmaan jangid</b> <!-- 1 --></td>
</tr>
<tr>
<td>Daan De Meyer <!-- 1 --></td>
<td>Daniel Levin <!-- 1 --></td>
</tr>
<tr>
<td>David Edmundson <!-- 1 --></td>
<td>David Redondo <!-- 1 --></td>
</tr>
<tr>
<td><b>Felix Yan</b> <!-- 1 --></td>
<td>Filip Gawin <!-- 1 --></td>
</tr>
<tr>
<td><b>Frederik Banning</b> <!-- 1 --></td>
<td><b>Gary Wang</b> <!-- 1 --></td>
</tr>
<tr>
<td>Gleb Popov <!-- 1 --></td>
<td>Heiko Becker <!-- 1 --></td>
</tr>
<tr>
<td><b>Jan Przybylak</b> <!-- 1 --></td>
<td><b>Johnny Jazeix</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Juan Francisco Cantero Hurtado</b> <!-- 1 --></td>
<td>Luca Beltrame <!-- 1 --></td>
</tr>
<tr>
<td><b>Mario Aichinger</b> <!-- 1 --></td>
<td><b>Marvin Ahlgrimm</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Matheus C. França</b> <!-- 1 --></td>
<td>Michael Hansen <!-- 1 --></td>
</tr>
<tr>
<td><b>Mikhail Zolotukhin</b> <!-- 1 --></td>
<td><b>Momo Cao</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Nazar Kalinowski</b> <!-- 1 --></td>
<td><b>Peter J. Mello</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Peter Mello</b> <!-- 1 --></td>
<td>Philipp A <!-- 1 --></td>
</tr>
<tr>
<td><b>Raphael Rosch</b> <!-- 1 --></td>
<td><b>Robert-André Mauchin</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Samu Voutilainen</b> <!-- 1 --></td>
<td>Simon Persson <!-- 1 --></td>
</tr>
<tr>
<td>Volker Krause <!-- 1 --></td>
<td>Weng Xuetian <!-- 1 --></td>
</tr>
<tr>
<td><b>Yunhe Guo</b> <!-- 1 --></td>
<td><b>aa bb</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>hololeap hololeap</b> <!-- 1 --></td>
<td>Ömer Fadıl USTA <!-- 1 --></td>
</table>

## Contributors during the project lifetime
During the full project lifetime <b>571 people</b> contributed.
Thanks for making Kate possible!

<table>
<tr>
<td>Christoph Cullmann <!-- 5538 --></td>
<td>Dominik Haumann <!-- 2571 --></td>
</tr>
<tr>
<td>Joseph Wenninger <!-- 538 --></td>
<td>Erlend Hamberg <!-- 514 --></td>
</tr>
<tr>
<td>Anders Lund <!-- 513 --></td>
<td>Hamish Rodda <!-- 482 --></td>
</tr>
<tr>
<td>Kåre Särs <!-- 466 --></td>
<td>Simon St James <!-- 456 --></td>
</tr>
<tr>
<td>Alex Turbov <!-- 399 --></td>
<td>Volker Krause <!-- 363 --></td>
</tr>
<tr>
<td>Milian Wolff <!-- 361 --></td>
<td>Laurent Montel <!-- 350 --></td>
</tr>
<tr>
<td>Michal Humpula <!-- 332 --></td>
<td>David Nolden <!-- 269 --></td>
</tr>
<tr>
<td>Bernhard Beschow <!-- 219 --></td>
<td>Shaheed Haque <!-- 208 --></td>
</tr>
<tr>
<td>David Faure <!-- 171 --></td>
<td>Mark Nauwelaerts <!-- 171 --></td>
</tr>
<tr>
<td>Nibaldo González <!-- 168 --></td>
<td>Waqar Ahmed <!-- 166 --></td>
</tr>
<tr>
<td>Albert Astals Cid <!-- 165 --></td>
<td>Pascal Létourneau <!-- 165 --></td>
</tr>
<tr>
<td>Miquel Sabaté <!-- 163 --></td>
<td>Sven Brauch <!-- 155 --></td>
</tr>
<tr>
<td>Friedrich W. H. Kossebau <!-- 138 --></td>
<td>Jonathan Poelen <!-- 135 --></td>
</tr>
<tr>
<td>Burkhard Lück <!-- 134 --></td>
<td>Sebastian Pipping <!-- 121 --></td>
</tr>
<tr>
<td>Pino Toscano <!-- 108 --></td>
<td>Yuri Chornoivan <!-- 107 --></td>
</tr>
<tr>
<td>T.C. Hollingsworth <!-- 96 --></td>
<td>Christian Ehrlicher <!-- 93 --></td>
</tr>
<tr>
<td>Dirk Mueller <!-- 93 --></td>
<td>Robin Pedersen <!-- 93 --></td>
</tr>
<tr>
<td>Wilbert Berendsen <!-- 93 --></td>
<td>Pablo Martín <!-- 88 --></td>
</tr>
<tr>
<td>Michel Ludwig <!-- 87 --></td>
<td>Matthew Woehlke <!-- 84 --></td>
</tr>
<tr>
<td>loh tar <!-- 81 --></td>
<td>John Firebaugh <!-- 80 --></td>
</tr>
<tr>
<td>Alex Neundorf <!-- 74 --></td>
<td>Kevin Funk <!-- 72 --></td>
</tr>
<tr>
<td>Svyatoslav Kuzmich <!-- 66 --></td>
<td>Adrian Lungu <!-- 62 --></td>
</tr>
<tr>
<td>Christoph Feck <!-- 57 --></td>
<td>Christian Couder <!-- 56 --></td>
</tr>
<tr>
<td>Stephan Binner <!-- 56 --></td>
<td>Stephan Kulow <!-- 55 --></td>
</tr>
<tr>
<td>Mirko Stocker <!-- 54 --></td>
<td>Jesse Yurkovich <!-- 52 --></td>
</tr>
<tr>
<td>Alex Merry <!-- 51 --></td>
<td>Thomas Friedrichsmeier <!-- 51 --></td>
</tr>
<tr>
<td>Jan Paul Batrina <!-- 49 --></td>
<td>Vegard Øye <!-- 49 --></td>
</tr>
<tr>
<td>Malcolm Hunter <!-- 46 --></td>
<td>Alexander Neundorf <!-- 44 --></td>
</tr>
<tr>
<td>Ahmad Samir <!-- 42 --></td>
<td>Gregor Mi <!-- 42 --></td>
</tr>
<tr>
<td>Montel Laurent <!-- 42 --></td>
<td>Rafael Fernández López <!-- 38 --></td>
</tr>
<tr>
<td>Marco Mentasti <!-- 35 --></td>
<td>Simon Huerlimann <!-- 35 --></td>
</tr>
<tr>
<td>Carl Schwan <!-- 34 --></td>
<td>Urs Wolfer <!-- 33 --></td>
</tr>
<tr>
<td>Allen Winter <!-- 32 --></td>
<td>Aleix Pol <!-- 31 --></td>
</tr>
<tr>
<td>Phil Schaf <!-- 30 --></td>
<td>Jakob Petsovits <!-- 29 --></td>
</tr>
<tr>
<td>Andrew Coles <!-- 28 --></td>
<td>Gerald Senarclens de Grancy <!-- 28 --></td>
</tr>
<tr>
<td>Adriaan de Groot <!-- 27 --></td>
<td>Chusslove Illich <!-- 27 --></td>
</tr>
<tr>
<td>Thomas Fjellstrom <!-- 27 --></td>
<td>Andreas Hartmetz <!-- 26 --></td>
</tr>
<tr>
<td>Martijn Klingens <!-- 26 --></td>
<td>André Wöbbeking <!-- 24 --></td>
</tr>
<tr>
<td>Alex Richardson <!-- 22 --></td>
<td>Jarosław Staniek <!-- 22 --></td>
</tr>
<tr>
<td>Patrick Spendrin <!-- 22 --></td>
<td>Frederik Schwarzer <!-- 21 --></td>
</tr>
<tr>
<td>Nicolas Goutte <!-- 21 --></td>
<td>Alexander Lohnau <!-- 20 --></td>
</tr>
<tr>
<td>Bram Schoenmakers <!-- 20 --></td>
<td>Simon Hausmann <!-- 20 --></td>
</tr>
<tr>
<td>Tomáš Trnka <!-- 20 --></td>
<td>Adrián Chaves Fernández (Gallaecio) <!-- 19 --></td>
</tr>
<tr>
<td>Aurélien Gâteau <!-- 19 --></td>
<td>Aaron J. Seigo <!-- 18 --></td>
</tr>
<tr>
<td>Andreas Kling <!-- 18 --></td>
<td>Arto Hytönen <!-- 17 --></td>
</tr>
<tr>
<td>Clarence Dang <!-- 17 --></td>
<td>Niko Sams <!-- 17 --></td>
</tr>
<tr>
<td>Paul Giannaros <!-- 17 --></td>
<td>Andrew Paseltiner <!-- 16 --></td>
</tr>
<tr>
<td>Brian Anderson <!-- 16 --></td>
<td>Ian Reinhart Geiser <!-- 16 --></td>
</tr>
<tr>
<td>Frederik Gladhorn <!-- 15 --></td>
<td>Kevin Ottens <!-- 15 --></td>
</tr>
<tr>
<td>Matt Rogers <!-- 15 --></td>
<td>Thiago Macieira <!-- 14 --></td>
</tr>
<tr>
<td>Daniel Naber <!-- 13 --></td>
<td>Eike Hein <!-- 13 --></td>
</tr>
<tr>
<td>Jan Villat <!-- 13 --></td>
<td>Kai Uwe Broulik <!-- 13 --></td>
</tr>
<tr>
<td>Luca Beltrame <!-- 13 --></td>
<td>Luigi Toscano <!-- 13 --></td>
</tr>
<tr>
<td>Waldo Bastian <!-- 13 --></td>
<td>Daan De Meyer <!-- 12 --></td>
</tr>
<tr>
<td>Johannes Sixt <!-- 12 --></td>
<td>Jonathan Riddell <!-- 12 --></td>
</tr>
<tr>
<td>José Pablo Ezequiel Fernández <!-- 12 --></td>
<td>Leo Savernik <!-- 12 --></td>
</tr>
<tr>
<td>Andreas Cord-Landwehr <!-- 11 --></td>
<td>Andreas Pakulat <!-- 11 --></td>
</tr>
<tr>
<td>Andrey Matveyakin <!-- 11 --></td>
<td>Anthony Fieroni <!-- 11 --></td>
</tr>
<tr>
<td>Chusslove Illich (Часлав Илић) <!-- 11 --></td>
<td>Diego Iastrubni <!-- 11 --></td>
</tr>
<tr>
<td>Heiko Becker <!-- 11 --></td>
<td>Philipp A <!-- 11 --></td>
</tr>
<tr>
<td>Tobias Koenig <!-- 11 --></td>
<td>Alexander Potashev <!-- 10 --></td>
</tr>
<tr>
<td>Christoph Roick <!-- 10 --></td>
<td>Eduardo Robles Elvira <!-- 10 --></td>
</tr>
<tr>
<td>Harald Fernengel <!-- 10 --></td>
<td>Jiri Pinkava <!-- 10 --></td>
</tr>
<tr>
<td>Jonathan Schmidt-Dominé <!-- 10 --></td>
<td>Matthias Kretz <!-- 10 --></td>
</tr>
<tr>
<td>Nicolas Fella <!-- 10 --></td>
<td>Tomaz Canabrava <!-- 10 --></td>
</tr>
<tr>
<td>Trevor Blight <!-- 10 --></td>
<td>Wang Kai <!-- 10 --></td>
</tr>
<tr>
<td>Christophe Giboudeaux <!-- 9 --></td>
<td>Ellis Whitehead <!-- 9 --></td>
</tr>
<tr>
<td>Harsh Kumar <!-- 9 --></td>
<td>Jaime Torres <!-- 9 --></td>
</tr>
<tr>
<td>Lasse Liehu <!-- 9 --></td>
<td>Maks Orlovich <!-- 9 --></td>
</tr>
<tr>
<td>Martin Walch <!-- 9 --></td>
<td>Oswald Buddenhagen <!-- 9 --></td>
</tr>
<tr>
<td>Andras Mantia <!-- 8 --></td>
<td>Christian Loose <!-- 8 --></td>
</tr>
<tr>
<td>David Edmundson <!-- 8 --></td>
<td>David Palser <!-- 8 --></td>
</tr>
<tr>
<td>Dmitry Risenberg <!-- 8 --></td>
<td>Frank Osterfeld <!-- 8 --></td>
</tr>
<tr>
<td>John Layt <!-- 8 --></td>
<td>John Tapsell <!-- 8 --></td>
</tr>
<tr>
<td>Oliver Kellogg <!-- 8 --></td>
<td>Primoz Anzur <!-- 8 --></td>
</tr>
<tr>
<td>Ralf Habacker <!-- 8 --></td>
<td>René J.V. Bertin <!-- 8 --></td>
</tr>
<tr>
<td>flying sheep <!-- 8 --></td>
<td>Abhishek Patil <!-- 7 --></td>
</tr>
<tr>
<td>Anne-Marie Mahfouf <!-- 7 --></td>
<td>Bastian Holst <!-- 7 --></td>
</tr>
<tr>
<td>David Jarvie <!-- 7 --></td>
<td>Filip Gawin <!-- 7 --></td>
</tr>
<tr>
<td>George Staikos <!-- 7 --></td>
<td>Jeroen Wijnhout <!-- 7 --></td>
</tr>
<tr>
<td>John Salatas <!-- 7 --></td>
<td>Kazuki Ohta <!-- 7 --></td>
</tr>
<tr>
<td>Matt Broadstone <!-- 7 --></td>
<td>Nate Graham <!-- 7 --></td>
</tr>
<tr>
<td>Orgad Shaneh <!-- 7 --></td>
<td>Peter Oberndorfer <!-- 7 --></td>
</tr>
<tr>
<td>Ryan Cumming <!-- 7 --></td>
<td>Yury G. Kudryashov <!-- 7 --></td>
</tr>
<tr>
<td>Adam Treat <!-- 6 --></td>
<td>Andrzej Dabrowski <!-- 6 --></td>
</tr>
<tr>
<td>Charles Samuels <!-- 6 --></td>
<td>David Schulz <!-- 6 --></td>
</tr>
<tr>
<td>Dawit Alemayehu <!-- 6 --></td>
<td>Gleb Popov <!-- 6 --></td>
</tr>
<tr>
<td>Hannah von Reth <!-- 6 --></td>
<td>Ian Wakeling <!-- 6 --></td>
</tr>
<tr>
<td>Jaison Lee <!-- 6 --></td>
<td>Jos van den Oever <!-- 6 --></td>
</tr>
<tr>
<td>Lukáš Tinkl <!-- 6 --></td>
<td>Martin T. H. Sandsmark <!-- 6 --></td>
</tr>
<tr>
<td>Massimo Callegari <!-- 6 --></td>
<td>Michael Jansen <!-- 6 --></td>
</tr>
<tr>
<td>Méven Car <!-- 6 --></td>
<td>Nathaniel Graham <!-- 6 --></td>
</tr>
<tr>
<td>Nick Shaforostoff <!-- 6 --></td>
<td>R.J.V. Bertin <!-- 6 --></td>
</tr>
<tr>
<td>Rafał Rzepecki <!-- 6 --></td>
<td>Safa AlFulaij <!-- 6 --></td>
</tr>
<tr>
<td>Samuel Gaist <!-- 6 --></td>
<td>Shubham Jangra <!-- 6 --></td>
</tr>
<tr>
<td>Weng Xuetian <!-- 6 --></td>
<td>York Xiang <!-- 6 --></td>
</tr>
<tr>
<td>Allan Sandfeld Jensen <!-- 5 --></td>
<td>Andreas Holzammer <!-- 5 --></td>
</tr>
<tr>
<td>Arno Rehn <!-- 5 --></td>
<td>Bernd Gehrmann <!-- 5 --></td>
</tr>
<tr>
<td>Carlo Segato <!-- 5 --></td>
<td>Carsten Pfeiffer <!-- 5 --></td>
</tr>
<tr>
<td>Charles Vejnar <!-- 5 --></td>
<td>Daniel Levin <!-- 5 --></td>
</tr>
<tr>
<td>David Herberth <!-- 5 --></td>
<td>David Redondo <!-- 5 --></td>
</tr>
<tr>
<td>Harri Porten <!-- 5 --></td>
<td>Harsh Chouraria J <!-- 5 --></td>
</tr>
<tr>
<td>Helio Castro <!-- 5 --></td>
<td>Heng Liu <!-- 5 --></td>
</tr>
<tr>
<td>Holger Danielsson <!-- 5 --></td>
<td>Ivan Čukić <!-- 5 --></td>
</tr>
<tr>
<td>Ivo Anjo <!-- 5 --></td>
<td>Michael Hansen <!-- 5 --></td>
</tr>
<tr>
<td>Nicolás Alvarez <!-- 5 --></td>
<td>Olivier Goffart <!-- 5 --></td>
</tr>
<tr>
<td>Peter Kümmel <!-- 5 --></td>
<td>Richard Smith <!-- 5 --></td>
</tr>
<tr>
<td>Scott Wheeler <!-- 5 --></td>
<td>Stephen Kelly <!-- 5 --></td>
</tr>
<tr>
<td>Thomas Braun <!-- 5 --></td>
<td>andreas kainz <!-- 5 --></td>
</tr>
<tr>
<td>Andi Fischer <!-- 4 --></td>
<td>Andrius Štikonas <!-- 4 --></td>
</tr>
<tr>
<td>Ben Cooksley <!-- 4 --></td>
<td>Benjamin Meyer <!-- 4 --></td>
</tr>
<tr>
<td>Darío Andrés Rodríguez <!-- 4 --></td>
<td>Dominique Devriese <!-- 4 --></td>
</tr>
<tr>
<td>Emeric Dupont <!-- 4 --></td>
<td>Evgeniy Ivanov <!-- 4 --></td>
</tr>
<tr>
<td>Flavio Castelli <!-- 4 --></td>
<td>Grzegorz Szymaszek <!-- 4 --></td>
</tr>
<tr>
<td>Guo Yunhe <!-- 4 --></td>
<td>Helio Chissini de Castro <!-- 4 --></td>
</tr>
<tr>
<td>Luboš Luňák <!-- 4 --></td>
<td>Michael Pyne <!-- 4 --></td>
</tr>
<tr>
<td>Nadeem Hasan <!-- 4 --></td>
<td>Nikita Sirgienko <!-- 4 --></td>
</tr>
<tr>
<td>Rolf Eike Beer <!-- 4 --></td>
<td>Stefan Asserhäll <!-- 4 --></td>
</tr>
<tr>
<td>Thomas Schoeps <!-- 4 --></td>
<td>Toshitaka Fujioka <!-- 4 --></td>
</tr>
<tr>
<td>Xaver Hugl <!-- 4 --></td>
<td>Aaron Puchert <!-- 3 --></td>
</tr>
<tr>
<td>Alex Crichton <!-- 3 --></td>
<td>Andreas Gratzer <!-- 3 --></td>
</tr>
<tr>
<td>Bernhard Loos <!-- 3 --></td>
<td>Boris Egorov <!-- 3 --></td>
</tr>
<tr>
<td>Dave Corrie <!-- 3 --></td>
<td>David Leimbach <!-- 3 --></td>
</tr>
<tr>
<td>Davide Bettio <!-- 3 --></td>
<td>Dāvis Mosāns <!-- 3 --></td>
</tr>
<tr>
<td>Ernesto Castellotti <!-- 3 --></td>
<td>Francis Laniel <!-- 3 --></td>
</tr>
<tr>
<td>Harald Sitter <!-- 3 --></td>
<td>Helge Deller <!-- 3 --></td>
</tr>
<tr>
<td>Holger Schröder <!-- 3 --></td>
<td>Ignacio Castaño Aguado <!-- 3 --></td>
</tr>
<tr>
<td>Ilya Konstantinov <!-- 3 --></td>
<td>Jacob Rideout <!-- 3 --></td>
</tr>
<tr>
<td>Laurence Withers <!-- 3 --></td>
<td>Marcus Camen <!-- 3 --></td>
</tr>
<tr>
<td>Markus Meik Slopianka <!-- 3 --></td>
<td>Martin Kostolný <!-- 3 --></td>
</tr>
<tr>
<td>Michael Goffioul <!-- 3 --></td>
<td>Mickael Marchand <!-- 3 --></td>
</tr>
<tr>
<td>Mike Harris <!-- 3 --></td>
<td>Raymond Wooninck <!-- 3 --></td>
</tr>
<tr>
<td>Rex Dieter <!-- 3 --></td>
<td>Reza Arbab <!-- 3 --></td>
</tr>
<tr>
<td>Richard Dale <!-- 3 --></td>
<td>Richard J. Moore <!-- 3 --></td>
</tr>
<tr>
<td>Robert Knight <!-- 3 --></td>
<td>Sergio Martins <!-- 3 --></td>
</tr>
<tr>
<td>Simon Persson <!-- 3 --></td>
<td>Vladimir Prus <!-- 3 --></td>
</tr>
<tr>
<td>Willy De la Court <!-- 3 --></td>
<td>Wolfgang Bauer <!-- 3 --></td>
</tr>
<tr>
<td>Ömer Fadıl USTA <!-- 3 --></td>
<td>Alexander Dymo <!-- 2 --></td>
</tr>
<tr>
<td>Alexander Zhigalin <!-- 2 --></td>
<td>Andrew Crouthamel <!-- 2 --></td>
</tr>
<tr>
<td>Antonio Larrosa Jimenez <!-- 2 --></td>
<td>Antonio Rojas <!-- 2 --></td>
</tr>
<tr>
<td>Arek Koz (Arusekk) <!-- 2 --></td>
<td>Arend van Beelen jr <!-- 2 --></td>
</tr>
<tr>
<td>Bernhard Rosenkraenzer <!-- 2 --></td>
<td>Bhushan Shah <!-- 2 --></td>
</tr>
<tr>
<td>Caleb Tennis <!-- 2 --></td>
<td>Casper Boemann <!-- 2 --></td>
</tr>
<tr>
<td>Chris Howells <!-- 2 --></td>
<td>Cornelius Schumacher <!-- 2 --></td>
</tr>
<tr>
<td>Craig Drummond <!-- 2 --></td>
<td>Cristian Oneț <!-- 2 --></td>
</tr>
<tr>
<td>Cédric Borgese <!-- 2 --></td>
<td>David Bryant <!-- 2 --></td>
</tr>
<tr>
<td>Dmitry Suzdalev <!-- 2 --></td>
<td>Elvis Angelaccio <!-- 2 --></td>
</tr>
<tr>
<td>Enrique Matías Sánchez <!-- 2 --></td>
<td>Fabian Kosmale <!-- 2 --></td>
</tr>
<tr>
<td>Fabian Vogt <!-- 2 --></td>
<td>Frans Englich <!-- 2 --></td>
</tr>
<tr>
<td>Frerich Raabe <!-- 2 --></td>
<td>Gene Thomas <!-- 2 --></td>
</tr>
<tr>
<td>George Florea Bănuș <!-- 2 --></td>
<td>Gioele Barabucci <!-- 2 --></td>
</tr>
<tr>
<td>Gregory S. Hayes <!-- 2 --></td>
<td>Hoàng Đức Hiếu <!-- 2 --></td>
</tr>
<tr>
<td>Hugo Pereira Da Costa <!-- 2 --></td>
<td>Huon Wilson <!-- 2 --></td>
</tr>
<tr>
<td>Ivan Shapovalov <!-- 2 --></td>
<td>Jaime Torres Amate <!-- 2 --></td>
</tr>
<tr>
<td>Jekyll Wu <!-- 2 --></td>
<td>Jens Dagerbo <!-- 2 --></td>
</tr>
<tr>
<td>Julien Antille <!-- 2 --></td>
<td>Jure Repinc <!-- 2 --></td>
</tr>
<tr>
<td>Kurt Pfeifle <!-- 2 --></td>
<td>Leonardo Finetti <!-- 2 --></td>
</tr>
<tr>
<td>Marcello Massaro <!-- 2 --></td>
<td>Markus Pister <!-- 2 --></td>
</tr>
<tr>
<td>Markus Slopianka <!-- 2 --></td>
<td>Maximilian Löffler <!-- 2 --></td>
</tr>
<tr>
<td>Michael Palimaka <!-- 2 --></td>
<td>Michel Hermier <!-- 2 --></td>
</tr>
<tr>
<td>Mickael Bosch <!-- 2 --></td>
<td>Miklos Marton <!-- 2 --></td>
</tr>
<tr>
<td>Nikolas Zimmermann <!-- 2 --></td>
<td>Petter Stokke <!-- 2 --></td>
</tr>
<tr>
<td>Phil Young <!-- 2 --></td>
<td>Pierre-Marie Pédrot <!-- 2 --></td>
</tr>
<tr>
<td>Raphael Kubo da Costa <!-- 2 --></td>
<td>Rob Buis <!-- 2 --></td>
</tr>
<tr>
<td>Robert Hoffmann <!-- 2 --></td>
<td>Roberto Raggi <!-- 2 --></td>
</tr>
<tr>
<td>Scott Lawrence <!-- 2 --></td>
<td>Sebastian Kügler <!-- 2 --></td>
</tr>
<tr>
<td>Sebastian Sauer <!-- 2 --></td>
<td>Sergey Kalinichev <!-- 2 --></td>
</tr>
<tr>
<td>Shaun Reich <!-- 2 --></td>
<td>Silas Lenz <!-- 2 --></td>
</tr>
<tr>
<td>Sven Leiber <!-- 2 --></td>
<td>Thomas Braxton <!-- 2 --></td>
</tr>
<tr>
<td>Thomas Häber <!-- 2 --></td>
<td>Thomas Leitner <!-- 2 --></td>
</tr>
<tr>
<td>Thomas Surrel <!-- 2 --></td>
<td>Thorsten Roeder <!-- 2 --></td>
</tr>
<tr>
<td>Tim Beaulen <!-- 2 --></td>
<td>Tim Hutt <!-- 2 --></td>
</tr>
<tr>
<td>Tobias C. Berner <!-- 2 --></td>
<td>Valentin Rouet <!-- 2 --></td>
</tr>
<tr>
<td>Vincent Belliard <!-- 2 --></td>
<td>Vincenzo Buttazzo <!-- 2 --></td>
</tr>
<tr>
<td>Volker Augustin <!-- 2 --></td>
<td>Wes H <!-- 2 --></td>
</tr>
<tr>
<td>Will Entriken <!-- 2 --></td>
<td>Zack Rusin <!-- 2 --></td>
</tr>
<tr>
<td>Алексей Шилин <!-- 2 --></td>
<td>Aaron Seigo <!-- 1 --></td>
</tr>
<tr>
<td>Aleix Pol Gonzalez <!-- 1 --></td>
<td>Alex Hermann <!-- 1 --></td>
</tr>
<tr>
<td>Alexander Schlarb <!-- 1 --></td>
<td>Alexander Volkov <!-- 1 --></td>
</tr>
<tr>
<td>Alexey Bogdanenko <!-- 1 --></td>
<td>Amit Kumar Jaiswal <!-- 1 --></td>
</tr>
<tr>
<td>Ana Beatriz Guerrero López <!-- 1 --></td>
<td>Anakim Border <!-- 1 --></td>
</tr>
<tr>
<td>Anders Ponga <!-- 1 --></td>
<td>Andre Heinecke <!-- 1 --></td>
</tr>
<tr>
<td>Andrea Canciani <!-- 1 --></td>
<td>Andrea Scarpino <!-- 1 --></td>
</tr>
<tr>
<td>Andreas Hohenegger <!-- 1 --></td>
<td>Andreas Simon <!-- 1 --></td>
</tr>
<tr>
<td>Andreas Sturmlechner <!-- 1 --></td>
<td>Andrew Chen <!-- 1 --></td>
</tr>
<tr>
<td>Andrey Karepin <!-- 1 --></td>
<td>Andrey S. Cherepanov <!-- 1 --></td>
</tr>
<tr>
<td>Andrius da Costa Ribas <!-- 1 --></td>
<td>André Marcelo Alvarenga <!-- 1 --></td>
</tr>
<tr>
<td>Andy Goossens <!-- 1 --></td>
<td>Antoni Bella Pérez <!-- 1 --></td>
</tr>
<tr>
<td>Arctic Ice Studio <!-- 1 --></td>
<td>Arnaud Ruiz <!-- 1 --></td>
</tr>
<tr>
<td>Arnold Dumas <!-- 1 --></td>
<td>Arnold Krille <!-- 1 --></td>
</tr>
<tr>
<td>Ashish Bansal <!-- 1 --></td>
<td>Axel Kittenberger <!-- 1 --></td>
</tr>
<tr>
<td>Ayushmaan jangid <!-- 1 --></td>
<td>Azat Khuzhin <!-- 1 --></td>
</tr>
<tr>
<td>Bart Ribbers <!-- 1 --></td>
<td>Barış Metin <!-- 1 --></td>
</tr>
<tr>
<td>Ben Blum <!-- 1 --></td>
<td>Benjamin Buch <!-- 1 --></td>
</tr>
<tr>
<td>Bernd Buschinski <!-- 1 --></td>
<td>Björn Peemöller <!-- 1 --></td>
</tr>
<tr>
<td>Brad Hards <!-- 1 --></td>
<td>Brendan Zabarauskas <!-- 1 --></td>
</tr>
<tr>
<td>Bruno Virlet <!-- 1 --></td>
<td>Casper van Donderen <!-- 1 --></td>
</tr>
<tr>
<td>Christoph Rüßler <!-- 1 --></td>
<td>Christopher Blauvelt <!-- 1 --></td>
</tr>
<tr>
<td>Claudio Bantaloukas <!-- 1 --></td>
<td>Conrad Hoffmann <!-- 1 --></td>
</tr>
<tr>
<td>Corey Richardson <!-- 1 --></td>
<td>Cédric Pasteur <!-- 1 --></td>
</tr>
<tr>
<td>Dan Vrátil <!-- 1 --></td>
<td>Daniel Laidig <!-- 1 --></td>
</tr>
<tr>
<td>Daniel Micay <!-- 1 --></td>
<td>David Rosca <!-- 1 --></td>
</tr>
<tr>
<td>David Smith <!-- 1 --></td>
<td>Denis Steckelmacher <!-- 1 --></td>
</tr>
<tr>
<td>Diana-Victoria Tiriplica <!-- 1 --></td>
<td>Diggory Hardy <!-- 1 --></td>
</tr>
<tr>
<td>Dirk Rathlev <!-- 1 --></td>
<td>Duncan Mac-Vicar Prett <!-- 1 --></td>
</tr>
<tr>
<td>Ede Rag <!-- 1 --></td>
<td>Ederag <!-- 1 --></td>
</tr>
<tr>
<td>Elias Probst <!-- 1 --></td>
<td>Emanuele Tamponi <!-- 1 --></td>
</tr>
<tr>
<td>Emmanuel Lepage Vallee <!-- 1 --></td>
<td>Ewald Snel <!-- 1 --></td>
</tr>
<tr>
<td>Federico Zenith <!-- 1 --></td>
<td>Felix Yan <!-- 1 --></td>
</tr>
<tr>
<td>Filipe Saraiva <!-- 1 --></td>
<td>Francis Herne <!-- 1 --></td>
</tr>
<tr>
<td>Francois-Xavier Duranceau <!-- 1 --></td>
<td>Frederik Banning <!-- 1 --></td>
</tr>
<tr>
<td>Fredrik Höglund <!-- 1 --></td>
<td>Gary Wang <!-- 1 --></td>
</tr>
<tr>
<td>Germain Garand <!-- 1 --></td>
<td>Gregor Tätzner <!-- 1 --></td>
</tr>
<tr>
<td>Guillermo Antonio Amaral Bastidas <!-- 1 --></td>
<td>Guillermo Molteni <!-- 1 --></td>
</tr>
<tr>
<td>Hartmut Goebel <!-- 1 --></td>
<td>Heinz Wiesinger <!-- 1 --></td>
</tr>
<tr>
<td>HeroesGrave <!-- 1 --></td>
<td>Héctor Mesa Jiménez <!-- 1 --></td>
</tr>
<tr>
<td>Ian Monroe <!-- 1 --></td>
<td>Ignat Semenov <!-- 1 --></td>
</tr>
<tr>
<td>Ivan Koveshnikov <!-- 1 --></td>
<td>Jan Grulich <!-- 1 --></td>
</tr>
<tr>
<td>Jan Przybylak <!-- 1 --></td>
<td>Jeremy Whiting <!-- 1 --></td>
</tr>
<tr>
<td>Jesse Crossen <!-- 1 --></td>
<td>Jochen Wilhelmy <!-- 1 --></td>
</tr>
<tr>
<td>Joerg Schiermeier <!-- 1 --></td>
<td>John Schroeder <!-- 1 --></td>
</tr>
<tr>
<td>Johnny Jazeix <!-- 1 --></td>
<td>Jonathan L. Verner <!-- 1 --></td>
</tr>
<tr>
<td>Jonathan Raphael Joachim Kolberg <!-- 1 --></td>
<td>Jonathan Singer <!-- 1 --></td>
</tr>
<tr>
<td>José Joaquín Atria <!-- 1 --></td>
<td>Juan Francisco Cantero Hurtado <!-- 1 --></td>
</tr>
<tr>
<td>Juliano F. Ravasi <!-- 1 --></td>
<td>Juraj Oravec <!-- 1 --></td>
</tr>
<tr>
<td>Karol Szwed <!-- 1 --></td>
<td>Kevin Ballard <!-- 1 --></td>
</tr>
<tr>
<td>Kishore Gopalakrishnan <!-- 1 --></td>
<td>Kurt Granroth <!-- 1 --></td>
</tr>
<tr>
<td>Kurt Hindenburg <!-- 1 --></td>
<td>Kyle S Horne <!-- 1 --></td>
</tr>
<tr>
<td>Laurent Cimon <!-- 1 --></td>
<td>Lauri Watts <!-- 1 --></td>
</tr>
<tr>
<td>Lays Rodrigues <!-- 1 --></td>
<td>Leandro Emanuel López <!-- 1 --></td>
</tr>
<tr>
<td>Leandro Santiago <!-- 1 --></td>
<td>Li-yao Xia <!-- 1 --></td>
</tr>
<tr>
<td>Liu Zhe <!-- 1 --></td>
<td>Maciej Mrozowski <!-- 1 --></td>
</tr>
<tr>
<td>Magnus Hoff <!-- 1 --></td>
<td>Malte Starostik <!-- 1 --></td>
</tr>
<tr>
<td>Manuel Tortosa <!-- 1 --></td>
<td>Marc Espie <!-- 1 --></td>
</tr>
<tr>
<td>Marco Martin <!-- 1 --></td>
<td>Marijn Kruisselbrink <!-- 1 --></td>
</tr>
<tr>
<td>Mario Aichinger <!-- 1 --></td>
<td>Markus Brenneis <!-- 1 --></td>
</tr>
<tr>
<td>Martin Gräßlin <!-- 1 --></td>
<td>Martin Klapetek <!-- 1 --></td>
</tr>
<tr>
<td>Martin Sandsmark <!-- 1 --></td>
<td>Martin Tobias Holmedahl Sandsmark <!-- 1 --></td>
</tr>
<tr>
<td>Marvin Ahlgrimm <!-- 1 --></td>
<td>Matheus C. França <!-- 1 --></td>
</tr>
<tr>
<td>Matt Carberry <!-- 1 --></td>
<td>Matthias Gerstner <!-- 1 --></td>
</tr>
<tr>
<td>Matthias Hoelzer-Kluepfel <!-- 1 --></td>
<td>Matthias Klumpp <!-- 1 --></td>
</tr>
<tr>
<td>Melchior Franz <!-- 1 --></td>
<td>Michael Drueing <!-- 1 --></td>
</tr>
<tr>
<td>Michael Heidelbach <!-- 1 --></td>
<td>Michael Matz <!-- 1 --></td>
</tr>
<tr>
<td>Michael Ritzert <!-- 1 --></td>
<td>Michal Srb <!-- 1 --></td>
</tr>
<tr>
<td>Mikhail Zolotukhin <!-- 1 --></td>
<td>Mikko Perttunen <!-- 1 --></td>
</tr>
<tr>
<td>Miquel Sabaté Solà <!-- 1 --></td>
<td>Momo Cao <!-- 1 --></td>
</tr>
<tr>
<td>Médéric Boquien <!-- 1 --></td>
<td>Nazar Kalinowski <!-- 1 --></td>
</tr>
<tr>
<td>Nico Kruber <!-- 1 --></td>
<td>Nicola Gigante <!-- 1 --></td>
</tr>
<tr>
<td>Nicolas Lécureuil <!-- 1 --></td>
<td>Nikolay Kultashev <!-- 1 --></td>
</tr>
<tr>
<td>Oleksandr Senkovych <!-- 1 --></td>
<td>Oliver Sander <!-- 1 --></td>
</tr>
<tr>
<td>Olivier CHURLAUD <!-- 1 --></td>
<td>Olivier Felt <!-- 1 --></td>
</tr>
<tr>
<td>Ovidiu-Florin BOGDAN <!-- 1 --></td>
<td>Paolo Borelli <!-- 1 --></td>
</tr>
<tr>
<td>Parker Coates <!-- 1 --></td>
<td>Patrick José Pereira <!-- 1 --></td>
</tr>
<tr>
<td>Paul Gideon Dann <!-- 1 --></td>
<td>Paulo Barreto <!-- 1 --></td>
</tr>
<tr>
<td>Paulo Moura Guedes <!-- 1 --></td>
<td>Pavel Pertsev <!-- 1 --></td>
</tr>
<tr>
<td>Pedro Gimeno <!-- 1 --></td>
<td>Per Winkvist <!-- 1 --></td>
</tr>
<tr>
<td>Peter J. Mello <!-- 1 --></td>
<td>Peter Mello <!-- 1 --></td>
</tr>
<tr>
<td>Peter Penz <!-- 1 --></td>
<td>Philippe Fremy <!-- 1 --></td>
</tr>
<tr>
<td>Rafał Miłecki <!-- 1 --></td>
<td>Ralf Jung <!-- 1 --></td>
</tr>
<tr>
<td>Ralf Nolden <!-- 1 --></td>
<td>Ramon Zarazua <!-- 1 --></td>
</tr>
<tr>
<td>Randy Kron <!-- 1 --></td>
<td>Raphael Rosch <!-- 1 --></td>
</tr>
<tr>
<td>Richard Mader <!-- 1 --></td>
<td>Robert Gruber <!-- 1 --></td>
</tr>
<tr>
<td>Robert-André Mauchin <!-- 1 --></td>
<td>Rohan Garg <!-- 1 --></td>
</tr>
<tr>
<td>Rolf Magnus <!-- 1 --></td>
<td>Roman Gilg <!-- 1 --></td>
</tr>
<tr>
<td>Ruediger Gad <!-- 1 --></td>
<td>Samu Voutilainen <!-- 1 --></td>
</tr>
<tr>
<td>Sandro Giessl <!-- 1 --></td>
<td>Sascha Cunz <!-- 1 --></td>
</tr>
<tr>
<td>Sean Gillespie <!-- 1 --></td>
<td>Shane Wright <!-- 1 --></td>
</tr>
<tr>
<td>Simone Scalabrino <!-- 1 --></td>
<td>Stefan Gehn <!-- 1 --></td>
</tr>
<tr>
<td>Steve Mokris <!-- 1 --></td>
<td>Sven Greb <!-- 1 --></td>
</tr>
<tr>
<td>Sven Lüppken <!-- 1 --></td>
<td>Thomas Capricelli <!-- 1 --></td>
</tr>
<tr>
<td>Thomas Horstmeyer <!-- 1 --></td>
<td>Thomas Jarosch <!-- 1 --></td>
</tr>
<tr>
<td>Thomas McGuire <!-- 1 --></td>
<td>Thomas Reitelbach <!-- 1 --></td>
</tr>
<tr>
<td>Till Schfer <!-- 1 --></td>
<td>Tim Jansen <!-- 1 --></td>
</tr>
<tr>
<td>Tom Albers <!-- 1 --></td>
<td>Tore Melangen Havn <!-- 1 --></td>
</tr>
<tr>
<td>Vladimír Vondruš <!-- 1 --></td>
<td>Waqar Ahmed        <!-- 1 --></td>
</tr>
<tr>
<td>Werner Trobin <!-- 1 --></td>
<td>Wilco Greven <!-- 1 --></td>
</tr>
<tr>
<td>Will Stephenson <!-- 1 --></td>
<td>Wojciech Stachurski <!-- 1 --></td>
</tr>
<tr>
<td>Wouter Becq <!-- 1 --></td>
<td>Yuen Hoe Lim <!-- 1 --></td>
</tr>
<tr>
<td>Yunhe Guo <!-- 1 --></td>
<td>Zhigalin Alexander <!-- 1 --></td>
</tr>
<tr>
<td>Zoe Clifford <!-- 1 --></td>
<td>aa bb <!-- 1 --></td>
</tr>
<tr>
<td>bombless <!-- 1 --></td>
<td>est 31 <!-- 1 --></td>
</tr>
<tr>
<td>gamazeps <!-- 1 --></td>
<td>hololeap hololeap <!-- 1 --></td>
</tr>
<tr>
<td>m.eik michalke <!-- 1 --></td>
<td>mdinger <!-- 1 --></td>
</tr>
<tr>
<td>tfry <!-- 1 --></td>
<td>visualfc <!-- 1 --></td>
</tr>
<tr>
<td>Émeric Dupont <!-- 1 --></td>
<td>Ömer Fadıl Usta <!-- 1 --></td>
</tr>
<tr>
<td>Сковорода Никита Андреевич <!-- 1 --></td>
</table>

