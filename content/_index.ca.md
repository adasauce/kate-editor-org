---
layout: index
---
El Kate és un editor multidocument que forma part del [KDE](https://kde.org) des del llançament 2.2. En ser una [aplicació del KDE](https://kde.org/applications), el Kate es distribueix amb transparència de xarxa, i també amb integració amb les característiques excepcionals del KDE. Trieu-lo per a visualitzar codi font HTML des del Konqueror, editar fitxers de configuració, escriure aplicacions noves o qualsevol altra tasca d'edició de text. Només necessitareu una instància del Kate en execució. [Vegeu més...](/about/)

![Captura de pantalla del Kate que mostra diversos documents i l'emulador de
terminal integrat](/images/kate-window.png)