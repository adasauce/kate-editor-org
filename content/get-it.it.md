---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Ottenere Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & sistemi Unix

* Installa [Kate](https://apps.kde.org/en/kate) o [KWrite](https://apps.kde.org/en/kwrite) dalla [tua distribuzione](https://kde.org/distributions/):

* [Kate da Discover o da altri negozi di applicazioni AppStream](appstream://org.kde.kate.desktop)

* [KWrite da Discover o da altri negozi di applicazioni AppStream](appstream://org.kde.kwrite.desktop)

* [Il pacchetto Flatpak di Kate in Flathub](https://flathub.org/apps/details/org.kde.kate)

* [Il pacchetto Snap di Kate in Snapcraft](https://snapcraft.io/kate)

* [Pacchetto AppImage della versione di Kate (64bit)](https://binary-factory.kde.org/job/Kate_Release_appimage/)

* [Pacchetto AppImage della versione nightly di Kate (64bit)](https://binary-factory.kde.org/job/Kate_Nightly_appimage/)

* [Compilalo](/build-it/#linux) dai sorgenti.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate nel Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

* [Kate da Chocolatey](https://chocolatey.org/packages/kate)

* [Installatore di Kate (64bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Installatore versione nightly di Kate (64bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Compilalo](/build-it/#windows) dai sorgenti.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Installatore di Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Installatore versione nightly di Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Compilalo](/build-it/#mac) dai sorgenti.