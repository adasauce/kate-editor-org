---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Obtener Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux y Unix

* Instalar [Kate](https://apps.kde.org/es/kate) o [KWrite](https://apps.kde.org/es/kwrite) en [su distribución](https://kde.org/distributions/)

{{< get-it-button-discover

button-kate-title="Instalar Kate" button-kate-subtitle="mediante Discover" button-kate-alt="Instalar Kate mediante Discover u otras tiendas de aplicaciones AppStream"

button-kwrite-title="Instalar KWrite" button-kwrite-subtitle="mediante Discover" button-kwrite-alt="Instalar KWrite mediante Discover u otras tiendas de aplicaciones AppStream"

>}}

Estos botones solo funciona con [Discover](https://apps.kde.org/es/discover) y otras tiendas de aplicaciones AppStream, como [GNOME Software](https://wiki.gnome.org/Apps/Software). También puede usar el gestor de paquetes de su distribución, como [Muon](https://apps.kde.org/es/muon) o [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

* [Paquete Flatpak de Kate en Flathub](https://flathub.org/apps/details/org.kde.kate)

{{< get-it-button link="https://flathub.org/apps/details/org.kde.kate" img="/images/flathub-badge-i-en.svg" width="200" alt="Download Kate on Flathub" />}}

* [Paquete Snap de Kate en Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-white.svg" width="200" alt="Get Kate on the Snap store" />}}

* [Paquete AppImage del último lanzamiento de Kate (64 bits)](https://binary-factory.kde.org/job/Kate_Release_appimage/) *

* [Paquete AppImage de la compilación diaria de Kate (64 bits)](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **

* [Compilar](/build-it/#linux) el código fuente.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate en la Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.svg" width="200" alt="Get Kate from the Microsoft store" />}}

* [Kate mediante Chocolatey](https://chocolatey.org/packages/kate)

* [Instalador del último lanzamiento de Kate (64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *

* [Instalador de la compilación diaria de Kate (64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **

* [Compilar](/build-it/#windows) el código fuente.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

* [Instalador del último lanzamiento de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *

* [Instalador de la compilación diaria de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **

* [Compilar](/build-it/#mac) el código fuente.

{{< /get-it >}}



{{< get-it-info >}}

**Acerca de los lanzamientos:** <br>[Kate](https://apps.kde.org/es/kate) y [KWrite](https://apps.kde.org/es/kwrite) forman parte de las [Aplicaciones de KDE](https://apps.kde.org), que [se publican en conjunto unas 3 veces al año](https://community.kde.org/Schedules). El [motor de edición de texto](/about-katepart/), los [temas de color](/themes/) y el [resaltado de sintaxis](/syntax/) los proporciona [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), que [se actualiza cada mes](https://community.kde.org/Schedules/Frameworks). Las nuevas versiones se anuncian [aquí](https://kde.org/announcements/).

\* Los paquetes del **último lanzamiento** contienen la última versión de Kate y de KDE Frameworks.

\*\* Los paquetes de la **compilación diaria** se compilan automáticamente cada día a partir del código fuente, por lo que pueden ser inestables y contener errores o funcionalidades incompletas. Solo se recomiendan para hacer pruebas.