---
title: Join Us
author: Christoph Cullmann
date: 2010-07-09T08:58:54+00:00
---

### How to help with Kate development

If you want to help us develop Kate, KWrite or KatePart, you should <a title="Kate/KWrite mailing list" href="https://mail.kde.org/mailman/listinfo/kwrite-devel" target="_blank">join our mailing list</a>. Contacting us at <a title="Kate IRC Channel" href="irc://irc.kde.org/kate" target="_blank">#kate on irc.kde.org</a> is also a good idea, we are always happy to help you get started.

### How to Build Kate

Kate can easily be build on a current Linux distribution, we provide a [how-to](/build-it/) for this.
For other operating systems, the [how-to](/build-it/) page provides extra links with information.

### Contributing code

For contributions, the best way to hand them in is via our GitLab instance [invent.kde.org](https://invent.kde.org/utilities/kate).
Just open a new [merge request](https://invent.kde.org/utilities/kate/merge_requests) there for Kate.

### Areas of work

The main work area is the programming, but if you are not a programmer you can help in many other areas:

  * Write and maintain documentation.
  * Write and maintain syntax highlight files.
  * Write and maintain scripts.
  * Maintain bug reports.
  * Provide us with useful feedback.
  * Help us by helping <a href="http://www.kde.org" target="_blank">KDE</a>. As a part of KDE, Kate benefits from the KDE as a whole, apart from the excellent integration provided by KDE the help from the <a href="http://i18n.kde.org" target="_blank">KDE translation team</a> is invaluable.

### C/C++ Coding Standards

The code in ktexteditor.git and kate.git shall follow the style described for <a href="https://techbase.kde.org/Policies/Kdelibs_Coding_Style" target="_blank">KDELIBS</a>.

### Documenting your code

We use Doxygen syntax to document code, and it&#8217;s nice to document everything, even private code, since other people than you might get to work on it or just try to understand it.

For comments inside of functions, C++ single-line comments are preferred over
multi-line C comments.

### Good starting points

Good ideas what to look at first are:

  * <a title="Kate Bugs" href="https://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor" target="_blank">Our bugs</a> in the KDE bugtracker
  * <a title="Kate Wishes" href="https://bugs.kde.org/buglist.cgi?product=frameworks-ktexteditor&product=kate&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist" target="_blank">Our wishes</a> in the KDE bugtracker

 [1]: mailto:kwrite-devel@kde.org
