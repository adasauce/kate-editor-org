---
title: Recursos

autor: Christoph Cullmann

data: 2010-07-09T08:40:19+00:00
---

## Recursos do aplicativo

![Captura de tela do recurso de dividir janelas do Kate e o plugin de
terminal](/images/kate-window.png)

+ Visualize e edite múltiplos documentos ao mesmo tempo dividindo a janela
horizontal e verticalmente + Vários plugins: [Terminal
embutido](https://konsole.kde.org), plugin do SQL, plugin de build, plugin
do GDB, Pesquisar e Substituir, e muito mais + Interface de documentos
múltiplos (MDI) + Suporte a sessões

## Recursos gerais

![Captura de tela do recurso de pesquisar e substituir do
Kate](/images/kate-search-replace.png)

+ Suporte a codificação (Unicode e vários outros) + Suporte à renderização
bidirecional do texto + Suporte ao Fim da linha (Windows, Unix, Mac) com
detecção automática + Transparência de rede (abrir arquivos remotos) +
Expansível por meio de scripts

## Recursos avançados do editor

![Captura de tela da borda do Kate com o número da linha
favoritos](/images/kate-border.png)

+ Sistema de favoritos (suporta também: pontos de quebra etc.) + Marcações
na barra de rolagem + Indicadores de modificação na linha + Números de linha
+ Dobragem de código

## Realce de sintaxe

![Captura de tela do recurso de realce de sintaxe do
Kate](/images/kate-syntax.png)

+ Suporte a realce de sintaxe para mais de 300 linguagens + Correspondência
de parênteses + Verificação ortográfica inteligente dinâmica + Realce de
palavras selecionadas

## Recursos de programação

![Captura de tela dos recursos de programação do
Kate](/images/kate-programming.png)

+ Auto-indentação configurável via scripts + Manipulação inteligente de
comentários + Auto-completação com dicas de argumentos + Modo de entrada Vi
+ Modo de seleção de blocos retangular

## Pesquisar e substituir

![Captura de tela do recurso de pesquisa incremental do
Kate](/images/kate-search.png)

+ Pesquisa incremental também conhecida como &#8220;sugestões em tempo
real&#8221; + Suporte a pesquisar e substituir para múltiplas linhas +
Suporte a expressões regulares + Pesquisar e substituir em múltiplos
arquivos abertos ou em disco

## Backup e restauração

![Captura de tela do recurso de recuperar de uma quebra do
Kate](/images/kate-crash.png)

+ Backups al salvar + Arquivos de swap para recuperar dados na quebra do
sistema + Sistema de resfazer / refazer
