---
title: About
author: Christoph Cullmann
date: 2010-07-08T07:55:24+00:00
---

The Kate project develops two main products: [KatePart][2], the advanced editor component which is used in numerous KDE applications requiring a text editing component, and [Kate][3], a MDI text editor application. In addition, we provide KWrite, a simple SDI editor shell which allows the user to select his/her favourite editor component.

### Kate

Kate is a multi-document editor part of KDE since release 2.2. Being a KDE application, Kate ships with network transparency, as well as integration with the outstanding features of KDE. Choose it for viewing HTML sources from konqueror, editing configuration files, writing new applications or any other text editing task. You still need just one running instance of Kate.

With a multi-view editor like Kate you get a lot of advantages. You can view several instances of the same document and all instances are synchronized. Or you can view more files at the same time for easy reference or simultaneous editing.

### KWrite

KWrite is a simple text editor application, allowing you to edit one file at the time per window. As Kate, KWrite uses the editor component [KatePart][2].KWrite simply provides the selected editor component with a window frame, and lets you open and save documents. KWrite shares all features the KatePart provides, look [here to get an overview][2].

### Licensing

Kate is released under the [GNU Lesser General Public License (LGPL) Version 2][4][
][5] Kate is part of the KDE project. How to get the entire source code is described in the article [Get It][6].

### A Bit of History

The Kate project started in December 2000 at sourceforge.net. In the first month Kate was known as &#8220;KCEdit&#8221;, with C standing for Cullmann, the last name of the author. Over the time other people joined the project, so that the name was changed from KCEdit to Kant. This name was motivated by the philosopher <a title="Immanuel Kant" href="http://en.wikipedia.org/wiki/Immanuel_Kant" target="_blank" rel="noopener noreferrer">Immanuel Kant</a> and was supposed to be more neutral and distinct compared to KEdit. Time went on&#8230; &#8230;and Kant got more and more popular with new developers joining the project. At that time Kant was officially included in the kdebase module of the KDE desktop as addon for the old KWrite application, which was quite a success. Now all KDE users had access to our beloved text editor! &#8230;the only big problem then was that the pronunciation of Kant sounds misleading in the English language for some people. This was the reason why the name had to be changed again &#8211; and the final name was **Kate** (KDE advanced text editor). You can read <a title="Kate History" href="/2010/08/15/kate-history/" target="_self" rel="noopener noreferrer">the full story here</a>.

 [2]: /about-katepart
 [3]: /about-kate
 [4]: http://www.gnu.org/copyleft/lesser.html
 [5]: /wp-content/uploads/2011/08/about_kate_new.png
 [6]: /get-it/ "Get Kate"
