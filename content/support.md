---
title: Get Help
author: Christoph Cullmann
date: 2010-07-09T08:23:52+00:00
---

Before looking for support, be sure to read the available documentation, whether that is the <a title="Kate Handbook" href="http://docs.kde.org/stable/en/applications/kate/index.html" target="_blank">application handbook</a>, developer documentation or other documentation. Also read the information pages about <a title="About Kate" href="/about-kate/" target="_self">Kate</a>, <a title="About KWrite" href="/about-kate/" target="_self">KWrite</a> and <a title="About Kate Part" href="/about-katepart/" target="_self">Kate Part</a>.

### Get in touch

When in need for help, you have the following options to get in touch with Kate developers and users:

  * Use our mailing list, <kwrite-devel@kde.org>.
    The mailing list is open, that is you do not need to be subscribed to contact us. Do not forget to ask to be CC&#8217;d in answers in that case. To subscribe, visit the <a title="Kate and KWrite Mailing List" href="http://mail.kde.org/mailman/listinfo/kwrite-devel" target="_blank">kwrite-devel mailing list</a>. For archives, go <a title="Kate and Kwrite Mailing List Archive" href="http://lists.kde.org/?l=kwrite-devel&r=1&w=2" target="_blank">here</a>. Mark Mail has a <a title="Mark Mail kwrite-devel list" href="http://markmail.org/search/?q=kwrite-devel#query:kwrite-devel%20list%3Aorg.kde.kwrite-devel%20order%3Adate-backward+page:1+state:facets" target="_blank">great overview</a> of our list, too.
  * Visit our IRC channel <a title="Kate IRC Channel" href="irc://irc.kde.org/kate" target="_blank">#kate at irc.kde.org</a>.
    You can often meet Kate developers there, and we are usually willing to help with user support requests as well as development questions. Alternatively, try <a title="KDE IRC Channel" href="irc://irc.kde.org/kde" target="_blank">#kde</a> for user support and <a title="IRC Channel for KDE Development" href="irc://irc.kde.org/kde-devel" target="_blank">#kde-devel</a> for development help.

Many of the bigger applications using Kate Part such as KDevelop or Kile have their own support channels, mailing lists, IRC channels and web forums. If you have a problem with one of these applications and you are not sure if this is a bug of the Kate Part then please first report it to them. Please prefer these public channels over contacting the authors in private.

### Using the KDE bug system

The KDE bug tracking system at <a title="KDE Bug Tracking System" href="http://bugs.kde.org" target="_blank">http://bugs.kde.org</a> is used for managing all bugs related to Kate. You can use the _Help->Report Bug_ menu item found in most KDE applications to report bugs or wishes. It will start the bug report wizard and fill in some information for you.

Be sure to include as much information as possible when reporting bugs, the better the report the better the chances we can fix the reported problem.
