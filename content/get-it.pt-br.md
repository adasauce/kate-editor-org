---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Baixe o Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux e Unix

* Instale o [Kate](https://apps.kde.org/en/kate) ou [KWrite](https://apps.kde.org/en/kwrite) de [sua distribuição](https://kde.org/distributions/)

{{< get-it-button-discover

button-kate-title="Instalar Kate" button-kate-subtitle="via Discover" button-kate-alt="Instalar Kate via Discover ou outras lojas de aplicativos AppStream"

button-kwrite-title="Instalar KWrite" button-kwrite-subtitle="via Discover" button-kwrite-alt="Instalar KWrite via Discover ou outras lojas de aplicativos AppStream"

>}}

Estes botões só funcionam com o [Discover](https://apps.kde.org/en/discover) e outras lojas de aplicativos AppStream, como o [GNOME Software](https://wiki.gnome.org/Apps/Software). Você também pode usar o gerenciador de pacotes de sua distribuição, como o [Muon](https://apps.kde.org/en/muon) ou [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

* [Pacote Flatpak do Kate no Flathub](https://flathub.org/apps/details/org.kde.kate)

{{< get-it-button link="https://flathub.org/apps/details/org.kde.kate" img="/images/flathub-badge-i-en.svg" width="200" alt="Download Kate on Flathub" />}}

* [Pacote Snap do Kate no Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-white.svg" width="200" alt="Get Kate on the Snap store" />}}

* [Pacote AppImage da versão lançada (64bit) do Kate](https://binary-factory.kde.org/job/Kate_Release_appimage/) *

* [Pacote AppImage da versão noturna (64bit) do Kate](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **

* [Compile-o](/build-it/#linux) a partir do código-fonte.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate na Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.svg" width="200" alt="Get Kate from the Microsoft store" />}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Instalador (64bits) da versão lançada do Kate](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *

* [Instalador (64bits) da versão noturna do Kate](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **

* [Compile-o](/build-it/#windows) a partir do código-fonte.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

* [Instalador da versão lançada do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *

* [Instalador da versão lançada do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **

* [Compile-o](/build-it/#mac) a partir do código-fonte.

{{< /get-it >}}



{{< get-it-info >}}

**About the releases:** <br>[Kate](https://apps.kde.org/en/kate) and [KWrite](https://apps.kde.org/en/kwrite) are part of [KDE Applications](https://apps.kde.org), which are [released typically 3 times a year en-masse](https://community.kde.org/Schedules). The [text editing engine](/about-katepart/), the [color themes](/themes/) and the [syntax highlighting](/syntax/) are provided by [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), which is [updated monthly](https://community.kde.org/Schedules/Frameworks). New releases are announced [here](https://kde.org/announcements/).

\* The **release** packages contain the latest version of Kate and KDE Frameworks.

\*\* The **nightly** packages are automatically compiled daily from source code, therefore they may be unstable and contain bugs or incomplete features. These are only recommended for testing purposes.