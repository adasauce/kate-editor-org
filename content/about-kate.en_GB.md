---
title: Features

author: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Application Features

![Screenshot showing Kate window splitting feature and terminal
plugin](/images/kate-window.png)

+ View and edit multiple documents at the same time, by splitting
horizontally and vertically the window + Lot of plugins: [Embedded
terminal](https://konsole.kde.org), SQL plugin, build plugin, GDB plugin,
Replace in Files, and more + Multi-document interface (MDI)  + Session
support

## General Features

![Screenshot showing Kate search and replace
feature](/images/kate-search-replace.png)

+ Encoding support (Unicode and lots of others)  + Bi-directional text
rendering support + Line ending support (Windows, Unix, Mac), including auto
detection + Network transparency (open remote files)  + Extensible through
scripting

## Advanced Editor Features

![Screenshot of Kate border with line number and
bookmark](/images/kate-border.png)

+ Bookmarking system (also supported: break points etc.)  + Scroll bar marks
+ Line modification indicators + Line numbers + Code folding

## Syntax Highlighting

![Screenshot of Kate syntax highlighting features](/images/kate-syntax.png)

+ Highlighting support for over 300 languages + Bracket matching + Smart
on-the-fly spell checking + Highlighting of selected words

## Programming Features

![Screenshot of Kate programming features](/images/kate-programming.png)

+ Scriptable auto indentation + Smart comment and uncomment handling + Auto
completion with argument hints + Vi input mode + Rectangular block selection
mode

## Search & Replace

![Screenshot of Kate incremental search feature](/images/kate-search.png)

+ Incremental search, also known as &#8220;find as you type&#8221; + Support
for multiline search & replace + Regular expression support + Search &
replace in multiple opened files or files on disk

## Backup and Restore

![Screenshot of Kate recover from crash feature](/images/kate-crash.png)

+ Backups on save + Swap files to recover data on system crash + Undo / redo
system
