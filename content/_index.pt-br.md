---
layout: index
---
O Kate é um editor multi-documentos parte do [KDE](https://kde.org) desde a versão 2.2. Sendo um [aplicativo do KDE](https://kde.org/applications), o Kate vem com transparência de rede, bem como integração com os excepcionais recursos do KDE. Escolha-o para visualizar fontes HTML a partir do Konqueror, editar arquivos de configuração, escrever novos aplicativos ou qualquer outra tarefa de edição de texto. Você ainda precisa de apenas uma instância do Kate. [Aprenda mais...](/about/)

![Captura de tela do Kate mostrando múltiplos documentos e o emulador de
terminal](/images/kate-window.png)