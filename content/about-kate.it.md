---
author: Christoph Cullmann
date: 2010-07-09 08:40:19
title: "Funzionalit\xE0"
---
## Funzionalità dell'applicazione

![Schermata che mostra la funzionalità di divisione della finestra di Kate e l'estensione del terminale](/images/kate-window.png)

* Visualizza e modifica contemporaneamente più documenti, dividendo orizzontalmente e verticalmente la finestra

* Tante estensioni: [Terminale incorporato](https://konsole.kde.org), estensione per SQL, per la compilazione, per GDB, Sostituisci nei file, e altro ancora

* Interfaccia multi-documento (MDI)

* Supporto della sessione

## Caratteristiche generali

![Schermata che mostra le funzioni di ricerca e sostituzione di Kate](/images/kate-search-replace.png)

* Supporto per la codifica (Unicode e molte altre)

* Supporto per la resa bidirezionale del testo

* Supporto per il fine riga (Windows, Unix, Mac), incluso il rilevamento automatico

* Trasparenza della rete (apre file remoti)

* Estendibile mediante script

## Funzionalità di editor avanzate

![Schermata dell'area laterale di Kate con numeri di riga e segnalibro](/images/kate-border.png)

* Sistema di segnalibri (supportati anche: punti di interruzione, ecc.)

* Contrassegni di scorrimento

* Indicatori di modifica delle righe

* Numeri di riga

* Raggruppamento del codice

## Evidenziazione della sintassi

![Schermata della funzionalità di evidenziazione della sintassi di Kate](/images/kate-syntax.png)

* Supporto per la evidenziazione di oltre 300 linguaggi

* Verifica delle parentesi

* Controllo ortografico al volo intelligente

* Evidenziazione di parole selezionate

## Funzionalità di programmazione

![Schermata delle funzionalità di programmazione di Kate](/images/kate-programming.png)

* Rientro automatico con script

* Gestione intelligente per le parti commentate e decommentate

* Completamento delle parole con suggerimento degli argomenti

* Modalità di inserimento Vi

* Modalità di selezione a blocchi rettangolare

## Cerca e sostituisci

![Schermata della funzionalità di ricerca incrementale di Kate](/images/kate-search.png)

* Ricerca incrementali, nota anche come &#8220;trova quel che digiti&#8221;

* Supporto per cerca e sostituisci multi-riga

* Supporto per espressioni regolari

* Cerca e sostituisci in più file aperti o file su disco

## Copia di sicurezza e ripristino

![Schermata della funzionalità di recupero da un crash di Kate](/images/kate-crash.png)

* Copie di sicurezza al salvataggio

* File di swap per recuperare i dati dopo un blocco del sistema

* Sistema annulla / rifai