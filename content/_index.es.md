---
layout: index
---
Kate es un editor multidocumento que forma parte de [KDE](https://kde.org) desde la versión 2.2. Al formar parte de las [Aplicaciones de KDE](https://kde.org/applications), Kate proporciona transparencia de red, además de integración con las excelentes funcionalidades de KDE. Úselo para ver código HTML en Konqueror, para editar archivos de configuración, escribir nuevas aplicaciones o para cualquier otra tarea de edición de archivos de texto. Solo necesitará una única instancia de Kate. [Saber más...](/about/)

![Captura de pantalla de Kate que muestra múltiples documentos y el emulador de
terminal de compilación](/images/kate-window.png)