---
title: KatePart
author: Christoph Cullmann
date: 2010-07-09T08:40:53+00:00
---

KatePart is a fast and feature-rich text editor component with many advanced features. It implements the KTextEditor interfaces, a common interface collection for text editor components in KDE, allowing it to be selected as the editor in applications that lets the user chose from different implementations, and it can use KTextEditor plug-ins.

KatePart is originally based on code from the KDE 1 richtext editor widget, but has been almost completely rewritten over the years. It is the default text editor widget in <a title="About Kate" href="/about-kate" target="_self">Kate and KWrite</a> and also the default to display text in [Konqueror][1]. A lot of other applications use it as text editing component, too, like [KDevelop][2].

[<img class="aligncenter size-full wp-image-1268" title="KatePart in Action" src="/wp-content/uploads/2010/07/kate_part.png" alt="" width="532" height="470" srcset="/wp-content/uploads/2010/07/kate_part.png 532w, /wp-content/uploads/2010/07/kate_part-300x265.png 300w" sizes="(max-width: 532px) 100vw, 532px" />][3]

### Features include

  * Syntax highlighting for more than 180 text formats, including popular programming languages like C/C++, Java, Ruby as well as markup such as HTML and XML.
  * Code completion interface that allows applications to provide text completion through plugins.
  * Code folding lets users hide parts of the text. Code folding can be defined by the syntax highlighting file.
  * Automatic indentation for various formats.
  * Scriptable using JavaScript or D-Bus.
  * Advanced comment handling.
  * Color scheme support including syntax color schemes.
  * Dynamic word wrap.
  * Bookmarks for easy navigation in the text.
  * Other mark types available for applications, for example for breadpoints or similar.
  * Support for UNIX, Windows and Mac <acronym title="End Of Line">EOL</acronym> styles.
  * Advanced automatic bracket completion.
  * Marks, line numbers and folding regions display in margin.
  * Built-in command line, which can be extended by plugins.
  * Configurable per document by variables (modelines) embedded in the document text.
  * Configurable per mimetype or mimetype group by user defined file modes.
  * Configurable shortcut scheme for the editor widget.
  * Spell checking of full or partial content.
  * Encoding support.
  * Encoding conversion.
  * Plug-in support through KTextEditor.
  * Export of highlighted text to HTML.
  * Printing with support for syntax highlighting, line numbers and configurable header/footer.

### Licensing

KatePart is developed under a Open Source license to keep it available to the public and usable for other Open Source projects.

It is part of the _kdelibs_ maintained by the [KDE Project][4] since KDE 3.0 and is released under the [GNU Lesser General Public License (LGPL) Version 2][5].

 [1]: http://www.konqueror.org
 [2]: http://www.kdevelop.org
 [3]: /wp-content/uploads/2010/07/kate_part.png
 [4]: http://www.kde.org
 [5]: http://www.gnu.org/copyleft/lesser.html
