---
title: Merge Requests
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
---

This pages provides an overview about the merge requests we accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

## Overall Accepted Merge Requests

- 91 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 152 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 186 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 12 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



## Accepted Merge Requests of 2021

- 42 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 20 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 54 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 4 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

### January 2021

- [Port away from Qt's forever](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/98)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the KTextEditor repository at creation day.

- [highlighter_benchmark: ignore .clang-format](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/161)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Do not restore Projects tool view when no project exists](https://invent.kde.org/utilities/kate/-/merge_requests/218)
    - Request authored by [Alexander Lohnau](https://invent.kde.org/alex).
    - Merged in the Kate repository at creation day.

- [Also set selected foreground in semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/213)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Show dir selection dialog for ProjectConfig->index](https://invent.kde.org/utilities/kate/-/merge_requests/217)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [quickopen: construct context menu on demand](https://invent.kde.org/utilities/kate/-/merge_requests/215)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Don't expand semantic highlighting ranges to prevent incorrect highlighting while typing](https://invent.kde.org/utilities/kate/-/merge_requests/214)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Increase maximum indentation width to 200](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/97)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [[Vimode] Do not switch view when changing case (~ command)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/96)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [Use atomic variables for lock free access](https://invent.kde.org/utilities/kate/-/merge_requests/212)
    - Request authored by [Anthony Fieroni](https://invent.kde.org/anthonyfieroni).
    - Merged in the Kate repository at creation day.

- [Add test case for range accessed after deletion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/95)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [Less vibrant operator color for Breeze themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/159)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository after one day.

- [improve threading](https://invent.kde.org/utilities/kate/-/merge_requests/211)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository at creation day.

- [Work/fixes](https://invent.kde.org/utilities/kate/-/merge_requests/210)
    - Request authored by [Alexander Lohnau](https://invent.kde.org/alex).
    - Merged in the Kate repository at creation day.

- [Work/clang format](https://invent.kde.org/utilities/kate/-/merge_requests/207)
    - Request authored by [Alexander Lohnau](https://invent.kde.org/alex).
    - Merged in the Kate repository after one day.

- [Search disk files multithreaded](https://invent.kde.org/utilities/kate/-/merge_requests/208)
    - Request authored by [Kåre Särs](https://invent.kde.org/sars).
    - Merged in the Kate repository at creation day.

- [Clean up command bar code](https://invent.kde.org/utilities/kate/-/merge_requests/209)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Konsole plugin: Sync terminal path when the current document's url changes (e.g. first document open, save as)](https://invent.kde.org/utilities/kate/-/merge_requests/205)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the Kate repository after one day.

- [Only show "There are no more chars for next bookmark" error when in vimode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/93)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [LSP: Fix Ctrl + click underline highlighting broken in #include context](https://invent.kde.org/utilities/kate/-/merge_requests/203)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [LSP: Fix highlighting not cleared after commenting code out](https://invent.kde.org/utilities/kate/-/merge_requests/202)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Update Haskell for settings.json](https://invent.kde.org/utilities/kate/-/merge_requests/201)
    - Request authored by [hololeap hololeap](https://invent.kde.org/hololeap).
    - Merged in the Kate repository at creation day.

- [Use new signal/slot syntax](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/89)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after one day.

- [Retain replacement text as long as the power search bar is not closed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/90)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [[vimode] Fix motion to matching item off-by-one](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/91)
    - Request authored by [Arusekk ­­](https://invent.kde.org/arusekk).
    - Merged in the KTextEditor repository at creation day.

- [Add Workaround for KateTabBar sometimes disappearing on session load](https://invent.kde.org/utilities/kate/-/merge_requests/199)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the Kate repository after one day.

- [KateBookMarks: modernise code base](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/88)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository at creation day.

- [Fix alpha channel being ignored when reading from config interface](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/87)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [Fix color loading for LSP client plugin](https://invent.kde.org/utilities/kate/-/merge_requests/198)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Work/ahmad/signal syntax 2](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/82)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after 3 days.

- [Use the new theme function to provide better semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/195)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Fixed colors for parameters for some themes](https://invent.kde.org/utilities/kate/-/merge_requests/197)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Improve the opening bracket preview](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/83)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after one day.

- [CommandRangeExpressionParser: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/85)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository at creation day.

- [avoid duplicated highlighting ranges that kill ARGB rendering](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/86)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [Atom Light, Breeze Dark/Light: new color for Operator ; Breeze Light: new color for ControlFlow](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/154)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 3 days.

- [Improve readability of Solarized dark theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/157)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [remove unnecessary captures with a dynamic rule](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/156)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [add const overloads for some accessors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/158)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Add missing HTML tag](https://invent.kde.org/utilities/kate/-/merge_requests/196)
    - Request authored by [Yunhe Guo](https://invent.kde.org/guoyunhe).
    - Merged in the Kate repository at creation day.

- [Fix translations](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/14)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the kate-editor.org repository at creation day.

- [expose the global KSyntaxHighlighting::Repository read-only](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/84)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [Correct indentation bug when line contains "for" or "else".](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/79)
    - Request authored by [Francis Laniel](https://invent.kde.org/eiffel).
    - Merged in the KTextEditor repository after 2 days.

- [Use editor theme colors to theme Search&Replace plugin](https://invent.kde.org/utilities/kate/-/merge_requests/194)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Fix extra inverted comma on autocomplete with autobrackets](https://invent.kde.org/utilities/kate/-/merge_requests/192)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Fix button translation in Get It page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/13)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the kate-editor.org repository at creation day.

- [Fix minor typo: This -> These](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/12)
    - Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor).
    - Merged in the kate-editor.org repository at creation day.

- [Fix compilation warning](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/81)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository at creation day.

- [Bash, Zsh: fix cmd;; in a case](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/155)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [try to ignore tabbar shortcuts](https://invent.kde.org/utilities/kate/-/merge_requests/189)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository after one day.

- [Draw tiny buttons for shortcuts instead of just text](https://invent.kde.org/utilities/kate/-/merge_requests/188)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Add a couple of extra checks for sanity and remove commented code](https://invent.kde.org/utilities/kate/-/merge_requests/191)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Improve Get It page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/11)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the kate-editor.org repository at creation day.

- [CMake: Use configure_file() to ensure noop incremental builds](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/80)
    - Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer).
    - Merged in the KTextEditor repository at creation day.

- [Add documentation for copy, cut and paste view functions.](https://invent.kde.org/utilities/kate/-/merge_requests/187)
    - Request authored by [Francis Laniel](https://invent.kde.org/eiffel).
    - Merged in the Kate repository at creation day.

- [Draw rectancle inline color notes including a hover effect](https://invent.kde.org/utilities/kate/-/merge_requests/181)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository after 2 days.

- [Correct an indentation bug.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/70)
    - Request authored by [Francis Laniel](https://invent.kde.org/eiffel).
    - Merged in the KTextEditor repository after 3 days.

- [Add a quick-open style command bar](https://invent.kde.org/utilities/kate/-/merge_requests/179)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Use erase/remove_if instead of loop to avoid massive random vector element removal costs](https://invent.kde.org/utilities/kate/-/merge_requests/185)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Optimize search some more and reduce memory usage](https://invent.kde.org/utilities/kate/-/merge_requests/184)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Tweak fuzzy matcher to score sequential matches a bit higher for kate command bar](https://invent.kde.org/utilities/kate/-/merge_requests/183)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Allow 'QMenu' inside the command bar](https://invent.kde.org/utilities/kate/-/merge_requests/182)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Work/clang format stuff](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/78)
    - Request authored by [Alexander Lohnau](https://invent.kde.org/alex).
    - Merged in the KTextEditor repository at creation day.

- [paint the small gap in selection color, if previous line end is in selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/77)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [Fix indent for when pressing enter and the function param has a comma at the end](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/76)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [avoid full line selection painting, more in line with other editors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/75)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [Port remaining uses of QRegExp in src/vimode into QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/67)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after 4 days.

- [Include QTest instead of QtTest (to avoid module-wide includes)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/74)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [email.xml: Detect nested comments and escaped characters](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/153)
    - Request authored by [Martin Walch](https://invent.kde.org/martinwalch).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Update atom light to use alpha colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/152)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Use an abstract item model for search and replace matches](https://invent.kde.org/utilities/kate/-/merge_requests/180)
    - Request authored by [Kåre Särs](https://invent.kde.org/sars).
    - Merged in the Kate repository at creation day.

- [Unbreak quickopen's reslect first](https://invent.kde.org/utilities/kate/-/merge_requests/178)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Avoid module-wide includes (QtTest and QtDBus)](https://invent.kde.org/utilities/kate/-/merge_requests/177)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the Kate repository at creation day.

- [Respect alpha colors while exporting html](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/73)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [Alpha colors support: Config layer](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/72)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [Current line highlighting over full icon border](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/71)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository after one day.

- [remap some Symbol and Operator styles to dsOperator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/150)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Bash: fix } in ${!xy*} and more Parameter Expansion Operator (# in ${#xy} ;...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/151)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Enable alpha channel for editor colors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/69)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [Allow using the alpha channel in theme colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/148)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository after 3 days.

- [Update monokai colors and fix incorrect blue](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/149)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Update Kconfig highlighter to Linux 5.9](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/144)
    - Request authored by [Martin Walch](https://invent.kde.org/martinwalch).
    - Merged in the KSyntaxHighlighting repository after 6 days.

- [Dont override the cursor for whole application when building](https://invent.kde.org/utilities/kate/-/merge_requests/175)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Fix current line highlight has a tiny gap at the beginning](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/68)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [Allow / separated pattern to filter same filename across different directories](https://invent.kde.org/utilities/kate/-/merge_requests/176)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Fix crash due to invalid pointer when jumping using CtrlClick](https://invent.kde.org/utilities/kate/-/merge_requests/174)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [[Vimode] Do not skip folded range when the motion lands inside](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/64)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after 4 days.

- [Port the (trivial) uses of QRegExp in normal vimode to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/62)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after 6 days.

- [C++: fix us suffix](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/147)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Bash: fix #5: $ at the end of a double quoted string](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/146)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Fix compilation on Kubuntu 20.04](https://invent.kde.org/utilities/kate/-/merge_requests/173)
    - Request authored by [Kåre Särs](https://invent.kde.org/sars).
    - Merged in the Kate repository at creation day.

- [VHDL: fix function, procedure, type range/units and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/145)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [LSP Client: Jump to Declaration / Definition on Control-click](https://invent.kde.org/utilities/kate/-/merge_requests/172)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [expose the KSyntaxHighlighting theme](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/66)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [add configChanged to KTextEditor::Editor, too, for global configuration changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/65)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [Improve the ctags-GotoSymbol plugin](https://invent.kde.org/utilities/kate/-/merge_requests/171)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Use fuzzy filter in kate lspclientplugin symbol view](https://invent.kde.org/utilities/kate/-/merge_requests/170)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Update breeze-dark.theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/143)
    - Request authored by [Andrey Karepin](https://invent.kde.org/akarepin).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Improve the UI for quick-open](https://invent.kde.org/utilities/kate/-/merge_requests/168)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [allow to access & alter the current theme via config interface](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/63)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [Dont show absouloute path for files in quick-open](https://invent.kde.org/utilities/kate/-/merge_requests/167)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Use fuzzy filter in kateprojectplugin](https://invent.kde.org/utilities/kate/-/merge_requests/166)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Fix html tags appearing in ctags-goto-symbol](https://invent.kde.org/utilities/kate/-/merge_requests/163)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after 2 days.

- [Improve text for goto symbol actions](https://invent.kde.org/utilities/kate/-/merge_requests/165)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Minor QRegularExpression optimisations](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/60)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository at creation day.

- [Raku: #7: fix symbols that starts with Q](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/141)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Support fuzzy matching for quick open/...](https://invent.kde.org/utilities/kate/-/merge_requests/140)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after 20 days.

- [Add KTextEditor::LineRange](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/57)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the KTextEditor repository after one day.

- [Cursor, Range: Add fromString(QStringView) overload](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/58)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the KTextEditor repository after one day.

- [Allow "Dynamic Word Wrap Align Indent" to be disabled](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/59)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [Add Oblivion color scheme from GtkSourceView/Pluma/gEdit](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/132)
    - Request authored by [ntninja](https://invent.kde.org/ntninja).
    - Merged in the KSyntaxHighlighting repository after 7 days.

- [[Vimode]Fix search inside fold ranges](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/53)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after 6 days.

- [[Vimode] Fix Macro Completion Replay](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/52)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after 8 days.

- [Improve search performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/56)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository at creation day.

- [batch matches together to avoid massive costs for repeated model update](https://invent.kde.org/utilities/kate/-/merge_requests/159)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository after one day.

- [UrlInfo: handle filenames starting with ':'](https://invent.kde.org/utilities/kate/-/merge_requests/134)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository after 21 days.

- [Cache colors outside the loop](https://invent.kde.org/utilities/kate/-/merge_requests/162)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [avoid stall at end of search if not expand all set + show intermediate match count](https://invent.kde.org/utilities/kate/-/merge_requests/161)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository at creation day.

- [allow to ignore binary files for project based search, too](https://invent.kde.org/utilities/kate/-/merge_requests/158)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository at creation day.

- [Dont reconstruct the same i18n string everytime without any reason](https://invent.kde.org/utilities/kate/-/merge_requests/157)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.



## Accepted Merge Requests of 2020

- 49 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 87 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

### December 2020

- [Variable expansion: Add support for %{Document:Variable:<name>}](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/55)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the KTextEditor repository after 2 days.

- [C++ Highlighting: QOverload and co.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/139)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Fix labels beginning with period not being highlighted in GAS](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/137)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Update replace display results to match the search results](https://invent.kde.org/utilities/kate/-/merge_requests/156)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [External Tools: Trim executable, name and command name](https://invent.kde.org/utilities/kate/-/merge_requests/155)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [External Tools: Track mimetype changes to enable/disable actions](https://invent.kde.org/utilities/kate/-/merge_requests/154)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Improve search performance by reducing the amount of work](https://invent.kde.org/utilities/kate/-/merge_requests/153)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [C++ highlighting: add qGuiApp macro](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/138)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Improve dracula theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/136)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Bash, Zsh: ! with if, while, until ; Bash: pattern style for ${var,patt} and ${var^patt}](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/135)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Show the line text in search results instead of line no & col](https://invent.kde.org/utilities/kate/-/merge_requests/149)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after 3 days.

- [Changed Go Language Server](https://invent.kde.org/utilities/kate/-/merge_requests/150)
    - Request authored by [Marvin Ahlgrimm](https://invent.kde.org/treagod).
    - Merged in the Kate repository after 2 days.

- [Show the dragged text when dragging](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/54)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository after one day.

- [LSP: Add support for textDocument/implementation](https://invent.kde.org/utilities/kate/-/merge_requests/152)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Add documetnation about external tools plugin](https://invent.kde.org/utilities/kate/-/merge_requests/151)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [fix #5: Bash, Zsh: comments within array](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/134)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Cucumber feature syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/133)
    - Request authored by [Samu Voutilainen](https://invent.kde.org/svoutilainen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Zsh: fix brace expansion in a command](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/131)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [add weakDeliminator and additionalDeliminator with keyword, WordDetect, Int,...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/111)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 29 days.

- [Add quick open go-to symbol support in CTags plugin](https://invent.kde.org/utilities/kate/-/merge_requests/145)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository after one day.

- [Highlight documentation: new attributes for some rules: weakDeliminator and additionalDeliminator](https://invent.kde.org/utilities/kate/-/merge_requests/147)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the Kate repository at creation day.

- [some fixes for Bash many fixes and improvements for Zsh](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/129)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Indexer: reset currentKeywords and currentContext when opening a new definition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/130)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Fix detach in TextRange::fixLookup()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/51)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository after one day.

- [Don't highlight currentLine if there is an overlapping selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/50)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository after one day.

- [Update Monokai theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/127)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Verify the correctness/presence of custom-styles in themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/128)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Add GitHub Dark/Light themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/126)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Add Inline Color Picker Plugin](https://invent.kde.org/utilities/kate/-/merge_requests/133)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the Kate repository after 10 days.

- [KateRegExpSearch: fix logic when adding '\n' between range lines](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/49)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after one day.

- [Fix: Backport changes by Andreas Gratzer in MR!113 to SPDX syntax template](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/119)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository after 13 days.

- [Add Atom One dark/light themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/125)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Fix monokai attribute & operator color](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/124)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Add StartupWMClass parameters to desktop files](https://invent.kde.org/utilities/kate/-/merge_requests/144)
    - Request authored by [Markus Slopianka](https://invent.kde.org/markuss).
    - Merged in the Kate repository at creation day.

- [Add Monokai color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/123)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [improve replace speed](https://invent.kde.org/utilities/kate/-/merge_requests/142)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository after one day.

- [CMake: Add missed 3.19 variables and some new added in 3.19.2](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/122)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Do not set deprecated CustomName property in knsrc file](https://invent.kde.org/utilities/kate/-/merge_requests/141)
    - Request authored by [Alexander Lohnau](https://invent.kde.org/alex).
    - Merged in the Kate repository after one day.

- [add an action to trigger copy & paste as one action](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/48)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository after 2 days.

- [Select first item in quick-open if only one item in list](https://invent.kde.org/utilities/kate/-/merge_requests/139)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Add item to tree after setting attributes](https://invent.kde.org/utilities/kate/-/merge_requests/138)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Fix some clazy warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/46)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository after one day.

- [feat: add text-wrap action icon for Dynamic Word Wrap](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/47)
    - Request authored by [Gary Wang](https://invent.kde.org/garywang).
    - Merged in the KTextEditor repository at creation day.

- [Improve search performance by a huge margin](https://invent.kde.org/utilities/kate/-/merge_requests/136)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Make Ctrl+W shortcut work in Ctrl+Tab tabswitcher](https://invent.kde.org/utilities/kate/-/merge_requests/135)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the Kate repository at creation day.

- [Undo indent in one step](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/45)
    - Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).
    - Merged in the KTextEditor repository after 3 days.

- [Pass parent to Q*Layout ctor instead of calling setLayout()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/44)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after 9 days.

- [Improvements of Java, Scala, Kotlin and Groovy](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/121)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [fix #5: && and || in a subcontext and fix function name pattern](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/120)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Remove unused include](https://invent.kde.org/utilities/kate/-/merge_requests/132)
    - Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).
    - Merged in the Kate repository after one day.

- [add capture attribute to the RegExpr rule. capture=0 (default) disables capture groups](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/118)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 4 days.

- [Bash: add (...), ||, && in [[ ... ]] ; add backquote in [ ... ] and [[ ... ]]](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/117)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [fix dependencies of the generated files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/116)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [lspclient: switch to a maintained javascript language server](https://invent.kde.org/utilities/kate/-/merge_requests/131)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after one day.

- [indexer: load all xml files in memory to simplify and fix the checkers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/107)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 11 days.

- [SPDX comments: fix broken highlighting for multi-line comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/113)
    - Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr).
    - Merged in the KSyntaxHighlighting repository after one day.

- [relaunch syntax generators when the source file is modified](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/114)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [C++ highlighting: update to Qt 5.15](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/115)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KSyntaxHighlighting repository at creation day.

### November 2020

- [systemd unit: update to systemd v247](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/112)
    - Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [ILERPG: simplify and test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/110)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 2 days.

- [filetree addon: make methods const where possible](https://invent.kde.org/utilities/kate/-/merge_requests/128)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository after 2 days.

- [ui.rc files: consistenly use <gui> instead of deprecated <kpartgui>](https://invent.kde.org/utilities/kate/-/merge_requests/129)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the Kate repository after one day.

- [Remove unused includes](https://invent.kde.org/utilities/kate/-/merge_requests/130)
    - Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).
    - Merged in the Kate repository after one day.

- [filetree addon: fix crash in filetree_model_test](https://invent.kde.org/utilities/kate/-/merge_requests/126)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository at creation day.

- [Zsh, Bash, Fish, Tcsh: add truncate and tsort in unixcommand keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/108)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Latex: some math environments can be nested](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/109)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Port some KComboBox usage to QComboBox](https://invent.kde.org/utilities/kate/-/merge_requests/125)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository at creation day.

- [Require KF 5.68](https://invent.kde.org/utilities/kate/-/merge_requests/124)
    - Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).
    - Merged in the Kate repository after one day.

- [Port KComboBox to QComboBox](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/43)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository at creation day.

- [Bash: many fixes and improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/105)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 3 days.

- [add --syntax-trace=stackSize](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/106)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 3 days.

- [Properly unset EDITOR env var, instead of setting it as empty.](https://invent.kde.org/utilities/kate/-/merge_requests/123)
    - Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry).
    - Merged in the Kate repository after one day.

- [php.xml: Fix matching endforeach](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/104)
    - Request authored by [Fabian Vogt](https://invent.kde.org/fvogt).
    - Merged in the KSyntaxHighlighting repository after 2 days.

- [Port to new KPluginMetaData-based KParts API](https://invent.kde.org/utilities/kate/-/merge_requests/121)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the Kate repository after 2 days.

- [Move bestThemeForApplicationPalette from KTextEditor here](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/103)
    - Request authored by [Xaver Hugl](https://invent.kde.org/zamundaaa).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Improve the automatic theme selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/41)
    - Request authored by [Xaver Hugl](https://invent.kde.org/zamundaaa).
    - Merged in the KTextEditor repository after one day.

- [Update README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/101)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Port away from deprecated Qt::MidButton](https://invent.kde.org/utilities/kate/-/merge_requests/120)
    - Request authored by [Robert-André Mauchin](https://invent.kde.org/rmauchin).
    - Merged in the Kate repository at creation day.

- [Improve get-it page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/9)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the kate-editor.org repository at creation day.

- [Update images in about-kate page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/10)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the kate-editor.org repository at creation day.

- [alert.xml: Add `NOQA` yet another popular alert in source code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/100)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [The "compact" core function is missing.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/99)
    - Request authored by [Vincenzo Buttazzo](https://invent.kde.org/buttazzo).
    - Merged in the KSyntaxHighlighting repository after one day.

- [The position:sticky value is missing.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/97)
    - Request authored by [Vincenzo Buttazzo](https://invent.kde.org/buttazzo).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [[Vimode] Prevent search box from disappearing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/40)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [Feature: Add the `comments.xml` as an umbrella syntax for various comment kinds](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/96)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Feature: Add `SPDX-Comments` syntax generator and XML](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/90)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository after 4 days.

### October 2020

- [KateModeMenuList: remove special margins for Windows](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/39)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KTextEditor repository at creation day.

- [Improvement: Add some more boolean values to `cmake.xml`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/93)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Fix: CMake syntax now marks `1` and `0` as special boolean values](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/95)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Improvement: Include Modelines rules in files where Alerts has been addded](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/94)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Misc: Add `SPDX-FileContributor` to `kateschema2theme`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/89)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository after 5 days.

- [Solarized themes: improve separator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/92)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Improvement: Updates for CMake 3.19](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/91)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository after 2 days.

- [Add support for the Zig language server](https://invent.kde.org/utilities/kate/-/merge_requests/117)
    - Request authored by [Matheus C. França](https://invent.kde.org/catarino).
    - Merged in the Kate repository after 5 days.

- [Add support for systemd unit files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/86)
    - Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr).
    - Merged in the KSyntaxHighlighting repository after 5 days.

- [Fix: Allow to `KActionSelector` @ filesystem browser config page to stretch vertically](https://invent.kde.org/utilities/kate/-/merge_requests/118)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the Kate repository at creation day.

- [angle bracket matching](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/38)
    - Request authored by [Milian Wolff](https://invent.kde.org/mwolff).
    - Merged in the KTextEditor repository at creation day.

- [Feature: Allow multiple `-s` options for `kateschema2theme` tool](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/88)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Improvement: Add various tests to the `kateschemsa2theme` converter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/87)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [filetree plugin: make filter case insensitive](https://invent.kde.org/utilities/kate/-/merge_requests/114)
    - Request authored by [Andreas Hartmetz](https://invent.kde.org/ahartmetz).
    - Merged in the Kate repository after 13 days.

- [kateschema2theme: Add a Python tool to convert old schema files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/85)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Correct a typo in README.md](https://invent.kde.org/utilities/kate/-/merge_requests/116)
    - Request authored by [Felix Yan](https://invent.kde.org/felixonmars).
    - Merged in the Kate repository at creation day.

- [Decrease opacity in separator of Breeze & Dracula themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/83)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Update README with "Color theme files" section](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/84)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Fix: Use `KDE_INSTALL_DATADIR` when install syntax files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/81)
    - Request authored by [Alex Turbov](https://invent.kde.org/turbov).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [kate: switch to recently used document when closing one](https://invent.kde.org/utilities/kate/-/merge_requests/115)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [Fix memory leak in KateMessageLayout](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/36)
    - Request authored by [Milian Wolff](https://invent.kde.org/mwolff).
    - Merged in the KTextEditor repository at creation day.

- [fix rendering of --syntax-trace=region with multiple graphics on the same offset](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/80)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 5 days.

- [fix some issues of fish shell](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/79)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 12 days.

- [ontheflycheck: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/33)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after 2 days.

- [katedocument: port one last QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/34)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after 2 days.

- [kateview: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/35)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after 2 days.

- [filetree plugin: make it possible to filter items in the tree view](https://invent.kde.org/utilities/kate/-/merge_requests/113)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository at creation day.

- [Cleanup unused comments in KNSRC file](https://invent.kde.org/utilities/kate/-/merge_requests/112)
    - Request authored by [Alexander Lohnau](https://invent.kde.org/alex).
    - Merged in the Kate repository at creation day.

- [Replace some RegExpr by StringDetect and StringDetect by Detect2Chars / DetectChar](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/78)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 5 days.

- [Compile without deprecated method against qt5.15](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/32)
    - Request authored by [Laurent Montel](https://invent.kde.org/mlaurent).
    - Merged in the KTextEditor repository after 2 days.

- [AppArmor: update highlighting for AppArmor 3.0](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/77)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [color cache for rgb to ansi256colors conversions (speeds up markdown loading)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/74)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [SELinux: use include keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/75)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [SubRip Subtitles & Logcat: small improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/76)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [generator for doxygenlua.xml](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/73)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Documentation fixes: Use more entities, some punctuation](https://invent.kde.org/utilities/kate/-/merge_requests/111)
    - Request authored by [Antoni Bella Pérez](https://invent.kde.org/bellaperez).
    - Merged in the Kate repository at creation day.

### September 2020

- [add open-with entry to the context menu of the filebrowser](https://invent.kde.org/utilities/kate/-/merge_requests/110)
    - Request authored by [Mario Aichinger](https://invent.kde.org/aichingm).
    - Merged in the Kate repository at creation day.

- [Add settings to toggle tabs close button and expansion](https://invent.kde.org/utilities/kate/-/merge_requests/109)
    - Request authored by [George Florea Bănuș](https://invent.kde.org/georgefb).
    - Merged in the Kate repository at creation day.

- [[kateprinter] Portaway from deprecated QPrinter methods](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/31)
    - Request authored by [Ömer Fadıl Usta](https://invent.kde.org/usta).
    - Merged in the KTextEditor repository after one day.

- [addons/externaltools: Really respect BUILD_TESTING](https://invent.kde.org/utilities/kate/-/merge_requests/108)
    - Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner).
    - Merged in the Kate repository at creation day.

- [externaltools: extend git blame command line](https://invent.kde.org/utilities/kate/-/merge_requests/107)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [Fix doxygen latex formulas](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/72)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after one day.

- [improve Kate config dialog](https://invent.kde.org/utilities/kate/-/merge_requests/105)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository after one day.

- [Use colorful "behavior" icon in settings window sidebar](https://invent.kde.org/utilities/kate/-/merge_requests/106)
    - Request authored by [Nate Graham](https://invent.kde.org/ngraham).
    - Merged in the Kate repository at creation day.

- [Don't create temporary buffer to detect mimetype for saved local file](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/30)
    - Request authored by [Milian Wolff](https://invent.kde.org/mwolff).
    - Merged in the KTextEditor repository at creation day.

- [ANSI highlighter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/70)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 8 days.

- [Port KRun to new KIO classes](https://invent.kde.org/utilities/kate/-/merge_requests/104)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository after one day.

- [Add Radical color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/68)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after 5 days.

- [make separator color less intrusive](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/71)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository after 3 days.

- [[Vimode]Improve presentation of unset buffers on error](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/27)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after 2 days.

- [move separator from between icon border and line numbers to between bar and text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/28)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository at creation day.

- [Add Nord color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/67)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [add proper license information to all themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/69)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [gruvbox theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/66)
    - Request authored by [Frederik Banning](https://invent.kde.org/laubblaeser).
    - Merged in the KSyntaxHighlighting repository after 2 days.

- [[Vimode]Make the behavior of numbered registers more accurate](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/26)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [Restore behavior of find selected when no selection is available](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/25)
    - Request authored by [Milian Wolff](https://invent.kde.org/mwolff).
    - Merged in the KTextEditor repository after one day.

- [Update configuration docs](https://invent.kde.org/utilities/kate/-/merge_requests/103)
    - Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor).
    - Merged in the Kate repository at creation day.

- [Clean up TwoViewCursor, open/close distinction has been redundant since 2007](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/24)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository at creation day.

- [Add ayu color theme (with light, dark and mirage variants)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/65)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Add POSIX/gmake alternate for Makefile simple variable assignment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/64)
    - Request authored by [Peter Mello](https://invent.kde.org/pmello).
    - Merged in the KSyntaxHighlighting repository after one day.

- [implement optional limit for number of tabs and LRU replacement](https://invent.kde.org/utilities/kate/-/merge_requests/101)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository after one day.

- [Bug:426451](https://invent.kde.org/utilities/kate/-/merge_requests/102)
    - Request authored by [Ayushmaan jangid](https://invent.kde.org/ayushmaanjangid).
    - Merged in the Kate repository at creation day.

- [Completely switch over to KSyntaxHighlighting::Theme usage](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/21)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository after 6 days.

- [tools to generate a graph from a syntax file](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/63)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [add shortcut for paste mouse selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/23)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KTextEditor repository at creation day.

- [Avoid some unneeded clones of snippet repositories.](https://invent.kde.org/utilities/kate/-/merge_requests/99)
    - Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry).
    - Merged in the Kate repository after 2 days.

- [Fix cancel button being focused](https://invent.kde.org/utilities/kate/-/merge_requests/100)
    - Request authored by [Alexander Lohnau](https://invent.kde.org/alex).
    - Merged in the Kate repository at creation day.

- [Move DocumentPrivate::attributeAt to KateViewInternal::attributeAt](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/22)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the KTextEditor repository at creation day.

- [Add unit tests for Alerts and Modelines](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/59)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Update README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/61)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Use KSyntaxHighlighting::Theme infrastructure for color themes in KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/20)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KTextEditor repository after 11 days.

- [Add Debian changelog and control example files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/57)
    - Request authored by [Samuel Gaist](https://invent.kde.org/sgaist).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [add the 'Dracula' color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/58)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [change theme json format, use meta object enum names for editor colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/54)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository after one day.

- [check kateversion >= 5.62 for fallthroughContext without fallthrough="true"...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/56)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Add syntax definition for todo.txt](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/55)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Fix Int, Float and HlC* rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/53)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 4 days.

- [Drop empty X-KDE-PluginInfo-Depends](https://invent.kde.org/utilities/kate/-/merge_requests/98)
    - Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).
    - Merged in the Kate repository at creation day.

### August 2020

- [rename 'Default' to 'Breeze Light'](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/52)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Varnish, Vala & TADS3: use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/51)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Proposal to improve the Vim Dark color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/50)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Add Vim Dark color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/49)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Ruby/Rails/RHTML: use the default color style and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/48)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [LDIF, VHDL, D, Clojure & ANS-Forth94: use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/47)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [ASP: use the default color style and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/46)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Various examples](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/45)
    - Request authored by [Samuel Gaist](https://invent.kde.org/sgaist).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Some optimizations on the loading of xml files and Rule::isWordDelimiter function](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/38)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 2 days.

- [SELinux CIL & Scheme: update bracket colors for dark themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/44)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [POV-Ray: use default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/43)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Test dark highlighting mode, too](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/39)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Django example](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/42)
    - Request authored by [Samuel Gaist](https://invent.kde.org/sgaist).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [CMake: fix illegible colors in dark themes and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/41)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/19)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the KTextEditor repository at creation day.

- [Extend R syntax highlighting test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/40)
    - Request authored by [Momo Cao](https://invent.kde.org/momocao).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [autotests: skip crashing tests if Qt doesn't have the fix](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/18)
    - Request authored by [David Faure](https://invent.kde.org/dfaure).
    - Merged in the KTextEditor repository after one day.

- [BrightScript: Add exception syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/35)
    - Request authored by [Daniel Levin](https://invent.kde.org/dlevin).
    - Merged in the KSyntaxHighlighting repository after 2 days.

- [Improve comments in some syntax definitions (Part 3)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/36)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [R Script: use default color style and small improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/37)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [API dox: use doxygen @warning tag to highlight warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/17)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the KTextEditor repository at creation day.

- [Do not style internal code comments with doxygen-triggering brackets /** */](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/16)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the KTextEditor repository at creation day.

- [Vimode: Make numbered registers and the small delete register behave more similar to vim](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/15)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [Remove obsolete COPYING files](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/14)
    - Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr).
    - Merged in the KTextEditor repository at creation day.

- [Improve comments in some syntax definitions (Part 2)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/32)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Remove obsolete COPYING files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/34)
    - Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Modelines: remove LineContinue rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/33)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Assembly languages: several fixes and more context highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/30)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Optimization: check if we are on a comment before using ##Doxygen which contains several RegExpr](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/31)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Make "search for selection" search if there is no selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/13)
    - Request authored by [Simon Persson](https://invent.kde.org/persson).
    - Merged in the KTextEditor repository at creation day.

- [Improve comments in some syntax definitions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/29)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after 3 days.

- [ColdFusion: use the default color style and replace some RegExpr rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/28)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after 3 days.

- [Port the search interface from QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/7)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the KTextEditor repository after 14 days.

- [Port away from KMimeTypeTrader](https://invent.kde.org/utilities/kate/-/merge_requests/97)
    - Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella).
    - Merged in the Kate repository after one day.

- [Vimode: Implement append-copy (Append to register instead of overwriting)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/11)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [KTextEditor: Convert copyright statements to SPDX expressions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/12)
    - Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr).
    - Merged in the KTextEditor repository at creation day.

- [Use angle brackets for context information](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/27)
    - Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Syntax-Highlighting: Convert copyright statements to SPDX expressions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/25)
    - Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Revert removal of byte-order-mark](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/26)
    - Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [xslt: change color of XSLT tags](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/24)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [txt2tags: improvements and fixes, use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/22)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Ruby, Perl, QML, VRML & xslt: use the default color style and improve comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/23)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [replace some Regexp with .*$ and add syntax tests](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/18)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 19 days.

- [Speed up a *lot* loading large files](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/8)
    - Request authored by [Tomaz  Canabrava](https://invent.kde.org/tcanabrava).
    - Merged in the KTextEditor repository after 5 days.

- [Add a text Zoom indicator](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/10)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository at creation day.

- [Show a preview of the matching open bracket's line (similar to the Arduino IDE)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/6)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the KTextEditor repository after 31 days.

- [Use consitently categorized logged warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/9)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the KTextEditor repository at creation day.

- [Allow more control over invocation of completion models when automatic invocation is not used.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/5)
    - Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry).
    - Merged in the KTextEditor repository after 35 days.

- [create StateData on demand](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/21)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Highlight documentation: improve description of attributes of the comment elements](https://invent.kde.org/utilities/kate/-/merge_requests/96)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the Kate repository at creation day.

- [Scheme: add datum comment, nested-comment and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/15)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 11 days.

- [Mathematica: some improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/19)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after 8 days.

- [Doxygen: fix some errors ; DoxygenLua: starts only with --- or --!](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/17)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 11 days.

- [AutoHotkey: complete rewrite](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/16)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 11 days.

- [Add syntax highlighting for Nim](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/20)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after 6 days.

- [Add support for the R language server](https://invent.kde.org/utilities/kate/-/merge_requests/95)
    - Request authored by [Luca Beltrame](https://invent.kde.org/lbeltrame).
    - Merged in the Kate repository after 3 days.

### July 2020

- [replace some RegExp by AnyChar, DetectChar, Detect2Chars or StringDetect](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/13)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 7 days.

- [language.xsd: remove HlCFloat and introduce char type](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/11)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 6 days.

- [KConfig: fix $(...) and operators + some improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/14)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 2 days.

- [ISO C++: fix performance in highlighting numbers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/10)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after 13 days.

- [Highlight documentation: add feature of comment position](https://invent.kde.org/utilities/kate/-/merge_requests/94)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the Kate repository after one day.

- [replace RegExpr=[.]{1,1} by DetectChar](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/12)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 4 days.

- [Lua: attribute with Lua54 and some other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/9)
    - Request authored by [Jonathan Poelen](https://invent.kde.org/jpoelen).
    - Merged in the KSyntaxHighlighting repository after 13 days.

- [Update D LSP Server](https://invent.kde.org/utilities/kate/-/merge_requests/93)
    - Request authored by [Ernesto Castellotti](https://invent.kde.org/ernytech).
    - Merged in the Kate repository at creation day.

- [Fix error if executable has spaces](https://invent.kde.org/utilities/kate/-/merge_requests/92)
    - Request authored by [Ernesto Castellotti](https://invent.kde.org/ernytech).
    - Merged in the Kate repository after one day.

- [kate: set last active view as current when restoring viewspace](https://invent.kde.org/utilities/kate/-/merge_requests/91)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

### June 2020

- [Make "goto line" work backwards](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/4)
    - Request authored by [Nazar Kalinowski](https://invent.kde.org/nazark).
    - Merged in the KTextEditor repository after one day.

- [Use QTabBar](https://invent.kde.org/utilities/kate/-/merge_requests/89)
    - Request authored by [Tomaz  Canabrava](https://invent.kde.org/tcanabrava).
    - Merged in the Kate repository after one day.

- [Add YARA language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/4)
    - Request authored by [Wes H](https://invent.kde.org/hurdw).
    - Merged in the KSyntaxHighlighting repository after 32 days.

- [Add Snort/Suricata language highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/5)
    - Request authored by [Wes H](https://invent.kde.org/hurdw).
    - Merged in the KSyntaxHighlighting repository after 27 days.

- [build-plugin: also accept + in message filename detection](https://invent.kde.org/utilities/kate/-/merge_requests/88)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after 3 days.

- [no deprecated binary json](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/8)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [session: sort list by ascending name](https://invent.kde.org/utilities/kate/-/merge_requests/87)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [JavaScript/TypeScript: highlight tags in templates](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/6)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after one day.

### May 2020

- [Add .diff to the file-changed-diff to enable mime detection on windows.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/2)
    - Request authored by [Kåre Särs](https://invent.kde.org/sars).
    - Merged in the KTextEditor repository after one day.

- [Fix "Run Current Document" so that it uses shebang or correct file path](https://invent.kde.org/utilities/kate/-/merge_requests/67)
    - Request authored by [Marcello Massaro](https://invent.kde.org/mmassaro).
    - Merged in the Kate repository after 99 days.

- [Preview addon: Better update delay logic.](https://invent.kde.org/utilities/kate/-/merge_requests/85)
    - Request authored by [aa bb](https://invent.kde.org/aabb).
    - Merged in the Kate repository after 6 days.

- [session: Sort session list by name again](https://invent.kde.org/utilities/kate/-/merge_requests/86)
    - Request authored by [Alex Hermann](https://invent.kde.org/alexhe).
    - Merged in the Kate repository after one day.

- [Raku: fix fenced code blocks in Markdown](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/3)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository after 6 days.

- [[gdbplugin] Port QRegExp to QRegular Expression](https://invent.kde.org/utilities/kate/-/merge_requests/83)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the Kate repository after 7 days.

- [[gdbplugin] Make extremely long variable names/values easier to view](https://invent.kde.org/utilities/kate/-/merge_requests/84)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the Kate repository after 6 days.

- [scrollbar minimap: performance: delay update for inactive documents](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/1)
    - Request authored by [Sven Brauch](https://invent.kde.org/brauch).
    - Merged in the KTextEditor repository after one day.

- [Add collaboration guide in README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/2)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the KSyntaxHighlighting repository at creation day.

- [Rename Perl6 to Raku and add new file extensions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/1)
    - Request authored by [Juan Francisco Cantero Hurtado](https://invent.kde.org/juanfra).
    - Merged in the KSyntaxHighlighting repository after one day.

- [Update debug target combobox when a target is selected](https://invent.kde.org/utilities/kate/-/merge_requests/82)
    - Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina).
    - Merged in the Kate repository at creation day.

- [Patch for Bug 408174. Bug when opening remote folder via SFTP.](https://invent.kde.org/utilities/kate/-/merge_requests/44)
    - Request authored by [Charles Vejnar](https://invent.kde.org/vejnar).
    - Merged in the Kate repository after 199 days.

- [Highlighting Doc: expand explanation about XML files directory](https://invent.kde.org/utilities/kate/-/merge_requests/76)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the Kate repository after 10 days.

- [Fix starting LSP server on Windows](https://invent.kde.org/utilities/kate/-/merge_requests/79)
    - Request authored by [Kåre Särs](https://invent.kde.org/sars).
    - Merged in the Kate repository at creation day.

### April 2020

- [Initialize overlooked variables (and remove redundant calls)](https://invent.kde.org/utilities/kate/-/merge_requests/74)
    - Request authored by [Filip Gawin](https://invent.kde.org/gawin).
    - Merged in the Kate repository after 20 days.

- [Highlighting Doc: add XML files directory of Kate's Flatpak/Snap package](https://invent.kde.org/utilities/kate/-/merge_requests/75)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the Kate repository at creation day.

### March 2020

- [lspclient: use pyls entry script for python LSP server](https://invent.kde.org/utilities/kate/-/merge_requests/71)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after 13 days.

- [build-plugin: pick and build default target when so requested](https://invent.kde.org/utilities/kate/-/merge_requests/72)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after 3 days.

- [[XMLTools Plugin] Port QRegExp to QRegularExpression](https://invent.kde.org/utilities/kate/-/merge_requests/62)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository after 47 days.

- [Use MarkInterfaceV2 to pass QIcons for the marker sidebar, not QPixmaps](https://invent.kde.org/utilities/kate/-/merge_requests/68)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the Kate repository after 19 days.

### February 2020

- [Session manager: empty session list](https://invent.kde.org/utilities/kate/-/merge_requests/70)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository at creation day.

- [Prefer data-error/warning/information icons over dialog-* for line marker](https://invent.kde.org/utilities/kate/-/merge_requests/69)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the Kate repository after 6 days.

- [search: handle both optional Project search options equally](https://invent.kde.org/utilities/kate/-/merge_requests/66)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after one day.

- [#417328 Properly declare supported file types](https://invent.kde.org/utilities/kate/-/merge_requests/64)
    - Request authored by [Jan Przybylak](https://invent.kde.org/janpr).
    - Merged in the Kate repository at creation day.

- [[BackTrace Plugin] QRegExp to QRegularExpression](https://invent.kde.org/utilities/kate/-/merge_requests/63)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository after one day.

- [KUserFeedback integration](https://invent.kde.org/utilities/kate/-/merge_requests/60)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository after 14 days.

- [Fix usage of QtSingleApplication on Windows and Apple](https://invent.kde.org/utilities/kate/-/merge_requests/61)
    - Request authored by [Kåre Särs](https://invent.kde.org/sars).
    - Merged in the Kate repository at creation day.

### January 2020

- [ColorSchemeChooser improvements](https://invent.kde.org/utilities/kate/-/merge_requests/59)
    - Request authored by [David Redondo](https://invent.kde.org/davidre).
    - Merged in the Kate repository after 4 days.

- [Fix External Tool "Google Selected Text"](https://invent.kde.org/utilities/kate/-/merge_requests/58)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [External tools: Correctly set the working directory](https://invent.kde.org/utilities/kate/-/merge_requests/57)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository after 2 days.

- [doc: extended lspclient documentation](https://invent.kde.org/utilities/kate/-/merge_requests/56)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [Highlight documentation: add equivalence in regex for number rules and add lookbehind assertions](https://invent.kde.org/utilities/kate/-/merge_requests/55)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the Kate repository at creation day.



## Accepted Merge Requests of 2019

- 45 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

### December 2019

- [Add icons to metadata of plugins supporting KDevelop/Plugin](https://invent.kde.org/utilities/kate/-/merge_requests/53)
    - Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau).
    - Merged in the Kate repository after 10 days.

- [LSP: add support for semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/47)
    - Request authored by [Milian Wolff](https://invent.kde.org/mwolff).
    - Merged in the Kate repository after 23 days.

- [doc: extend lspclient documentation](https://invent.kde.org/utilities/kate/-/merge_requests/52)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after 7 days.

- [[lspclient addon] Replace KRecursiveFilterProxyModel with QSortFilterProxyModel](https://invent.kde.org/utilities/kate/-/merge_requests/49)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository after 2 days.

- [Fix: Do not translate KActionCollection identifier](https://invent.kde.org/utilities/kate/-/merge_requests/50)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Fix translation of external tools](https://invent.kde.org/utilities/kate/-/merge_requests/51)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [[project addon] Don't use deprecated KRecursiveFilterProxyModel](https://invent.kde.org/utilities/kate/-/merge_requests/48)
    - Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).
    - Merged in the Kate repository after one day.

### November 2019

- [Read the user visible name of the default color scheme](https://invent.kde.org/utilities/kate/-/merge_requests/46)
    - Request authored by [David Redondo](https://invent.kde.org/davidre).
    - Merged in the Kate repository after 11 days.

- [Add external tools for formatting XML and JSON content](https://invent.kde.org/utilities/kate/-/merge_requests/45)
    - Request authored by [Volker Krause](https://invent.kde.org/vkrause).
    - Merged in the Kate repository at creation day.

- [Remove redundant and c style casts](https://invent.kde.org/utilities/kate/-/merge_requests/42)
    - Request authored by [Filip Gawin](https://invent.kde.org/gawin).
    - Merged in the Kate repository after 22 days.

### October 2019

- [Multi project ctags](https://invent.kde.org/utilities/kate/-/merge_requests/43)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [Update documentation of highlight & RegExp](https://invent.kde.org/utilities/kate/-/merge_requests/40)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the Kate repository at creation day.

- [Bump required version of qt to 5.10](https://invent.kde.org/utilities/kate/-/merge_requests/41)
    - Request authored by [Filip Gawin](https://invent.kde.org/gawin).
    - Merged in the Kate repository at creation day.

- [Project ctags](https://invent.kde.org/utilities/kate/-/merge_requests/36)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [External Tools: Translate name and category](https://invent.kde.org/utilities/kate/-/merge_requests/35)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository after 2 days.

- [kate: make size of persistent toolview also persistent](https://invent.kde.org/utilities/kate/-/merge_requests/31)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after 6 days.

- [Add language selector and make the homepage translatable](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/7)
    - Request authored by [Carl Schwan](https://invent.kde.org/carlschwan).
    - Merged in the kate-editor.org repository at creation day.

- [Projects Plugin: Support variables %{Project:Path} and %{Project:NativePath}](https://invent.kde.org/utilities/kate/-/merge_requests/34)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Add new custom value - KDE::windows_store](https://invent.kde.org/utilities/kate/-/merge_requests/33)
    - Request authored by [Carl Schwan](https://invent.kde.org/carlschwan).
    - Merged in the Kate repository at creation day.

### September 2019

- [fix hi-dpi rendering of tab buttons](https://invent.kde.org/utilities/kate/-/merge_requests/32)
    - Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann).
    - Merged in the Kate repository at creation day.

- [Compile with -DQT_DISABLE_DEPRECATED_BEFORE=0x050d00](https://invent.kde.org/utilities/kate/-/merge_requests/30)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Disalow Qt's Q_FOREACH/foreach macros](https://invent.kde.org/utilities/kate/-/merge_requests/29)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [LSP Client config page: Use ui file](https://invent.kde.org/utilities/kate/-/merge_requests/28)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [External Tools: Support adding actions to toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/23)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [External Tools: Extend output to include "Copy to Clipboard"](https://invent.kde.org/utilities/kate/-/merge_requests/27)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Properly translate the external tools menu + config widget](https://invent.kde.org/utilities/kate/-/merge_requests/26)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Add Tools > External Tools > Configure...](https://invent.kde.org/utilities/kate/-/merge_requests/25)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Implement KTextEditor::MainWindow::showPluginConfigPage()](https://invent.kde.org/utilities/kate/-/merge_requests/24)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [Remove deprecated plugins](https://invent.kde.org/utilities/kate/-/merge_requests/16)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository after 10 days.

- [build-plugin: handle and process ninja stdout](https://invent.kde.org/utilities/kate/-/merge_requests/22)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after one day.

- [Add a script to extract/merge translations](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/6)
    - Request authored by [Carl Schwan](https://invent.kde.org/carlschwan).
    - Merged in the kate-editor.org repository after 2 days.

- [reformat: also format source files](https://invent.kde.org/utilities/kate/-/merge_requests/20)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [tabswitcher: switch to next-in-line doc when top document is closed](https://invent.kde.org/utilities/kate/-/merge_requests/18)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after one day.

- [Added a line to set default sort order for the Symbolviewer.](https://invent.kde.org/utilities/kate/-/merge_requests/17)
    - Request authored by [Tore Havn](https://invent.kde.org/torehavn).
    - Merged in the Kate repository after one day.

- [Revive externaltools plugin](https://invent.kde.org/utilities/kate/-/merge_requests/15)
    - Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann).
    - Merged in the Kate repository at creation day.

- [search: make item margin configurable](https://invent.kde.org/utilities/kate/-/merge_requests/12)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after 2 days.

- [Avoid using `size` for checking emptiness](https://invent.kde.org/utilities/kate/-/merge_requests/13)
    - Request authored by [Filip Gawin](https://invent.kde.org/gawin).
    - Merged in the Kate repository at creation day.

- [Cleanup inheritance specifiers](https://invent.kde.org/utilities/kate/-/merge_requests/11)
    - Request authored by [Filip Gawin](https://invent.kde.org/gawin).
    - Merged in the Kate repository after one day.

- [Syntax Page: add generated PHP syntax](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/5)
    - Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez).
    - Merged in the kate-editor.org repository after 7 days.

### August 2019

- [Plugin search fixes](https://invent.kde.org/utilities/kate/-/merge_requests/10)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [Avoid copying if it's possible](https://invent.kde.org/utilities/kate/-/merge_requests/9)
    - Request authored by [Filip Gawin](https://invent.kde.org/gawin).
    - Merged in the Kate repository at creation day.

- [Add .clang-format file.](https://invent.kde.org/utilities/kate/-/merge_requests/7)
    - Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer).
    - Merged in the Kate repository after 3 days.

- [Buildplugin enhancements](https://invent.kde.org/utilities/kate/-/merge_requests/6)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository after 3 days.

- [Rename kate target to kate-lib to fix Windows compilation error](https://invent.kde.org/utilities/kate/-/merge_requests/5)
    - Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer).
    - Merged in the Kate repository at creation day.

- [Main CMake scripts cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/4)
    - Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer).
    - Merged in the Kate repository at creation day.

- [Further cleanup addons CMake scripts.](https://invent.kde.org/utilities/kate/-/merge_requests/3)
    - Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer).
    - Merged in the Kate repository at creation day.

- [katequickopen: add config option to present current or all project files](https://invent.kde.org/utilities/kate/-/merge_requests/2)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [build-plugin: also accept + in message filename detection](https://invent.kde.org/utilities/kate/-/merge_requests/1)
    - Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).
    - Merged in the Kate repository at creation day.

- [Add links to kde.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/4)
    - Request authored by [Carl Schwan](https://invent.kde.org/carlschwan).
    - Merged in the kate-editor.org repository after one day.

- [Fix margin in team page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/3)
    - Request authored by [Carl Schwan](https://invent.kde.org/carlschwan).
    - Merged in the kate-editor.org repository at creation day.

- [Add more information about kate in the homepage](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/1)
    - Request authored by [Carl Schwan](https://invent.kde.org/carlschwan).
    - Merged in the kate-editor.org repository at creation day.

