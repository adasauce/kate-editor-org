---
tytuł: Możliwości

autor: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Możliwości programu

![Zrzut ekranu pokazujący możliwość podziału okna Kate oraz wtyczka
terminalu](/images/kate-window.png)

+ Oglądaj i edytuj wiele dokumentów w tym samym czasie, poprzez podział okna
w pionie lub poziomie + wiele wtyczek: [osadzony
terminal](https://konsole.kde.org), wtyczka SQL, wtyczka budowania, wtyczka
GDB, Zastąp w plikach oraz więcej  + Interfejs wielu dokumentów (MDI)  +
Obsługa sesji

## Ogólne możliwości

![Zrzut ekranu pokazujący możliwość szukania i zastępowania
Kate](/images/kate-search-replace.png)

+ Obsługa kodowania (Unicode i wiele innych)  + Wyświetlanie dwukierunkowego
tekstu + Obsługa zakończeń wiersza (Windows, Unix, Mac) wraz z
samowykrywaniem + przezroczystość sieci (otwieranie zdalnych plików)  +
rozszerzalność poprzez skrypty

## Rozszerzone możliwości edytora

![Zrzut ekranu Kate pokazujący ramkę z numerami wierszy i
zakładkami](/images/kate-border.png)

+ system zakładek (także z obsługą punktów łamania itp.) + znaczniki na
pasku przewijania + wskaźniki zmian w wierszach + numery wierzy + zwijanie
kodu

## Podświetlanie składni

![Zrzut ekranu Kate pokazujący możliwości podświetlania
składni](/images/kate-syntax.png)

+ Obsługa podświetlania dla ponad 300 języków + dopasowywanie nawiasów +
mądre sprawdzanie pisowni w locie + podświetlanie zaznaczonych słów

## Możliwości programowania

![Zrzut ekranu Kate pokazujący możliwości
programowania](/images/kate-programming.png)

+ Skryptowe samowcinanie + Mądra obsługa dodawania uwag i ich usuwania +
samouzupełnianie z podpowiadaniem argumentów + tryb wprowadzania Vi + tryb
zaznaczania prostokątem

## Znajdź i zamień

![Zrzut ekranu Kate pokazujący możliwości przyrostowego
wyszukiwania](/images/kate-search.png)

+ Przyrostowe wyszukiwanie, znane także jako &#8220;szukaj podczas
pisania&#8221; + Obsługa wielowierszowego wyszukiwania i zastępowania +
obsługa wyrażeń regularnych + Szukaj i zastąp w wielu otwartych plikach lub
plikach na dysku

## Kopia zapasowa i przywracanie

![Zrzut ekranu Kate pokazujący możliwości powrotu z
usterki](/images/kate-crash.png)

+ kopie zapasowe przy zapisie + pliki wymiany do odzyskiwania danych po
wysypaniu systemu + system cofania/powracania
