---
layout: index
---
自 [KDE](https://kde.org) 软件套装 2.2 版开始，Kate 就一直是其中的一个多文档编辑器组件。作为一个 [KDE 应用程序](https://kde.org/applications)，Kate 自带网络透明性，还整合了各种 KDE 的出色功能。你可以用它在 Konqueror 中查看 HTML 源代码，编辑配置文件，从头编写应用程序、或者任何其他文本编辑工作——所有这些只需一个 Kate 实例即可全部搞定。[了解更多...](/about/)

![Kate 显示多个文档以及内置终端模拟器的屏幕截图](/images/kate-window.png)