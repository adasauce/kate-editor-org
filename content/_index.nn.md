---
layout: index
---
Kate er eit skrive­program frå [KDE](https://kde.org/) med støtte for fleire dokument i same vindauge. Og sidan det byggjer på [KDE-teknologi](https://kde.org/applications), er det integrert med andre KDE-program, slik at du for eksempel kan arbeida direkte med filer over nettet. Du kan bruka Kate til å visa HTML-kjelder frå Konqueror, redigera oppsett­filer, programmera nye program eller gjera anna redigering av reintekst­filer. Og du treng aldri ha meir enn éin Kate-instans oppe, sjølv om du redigerer fleire filer. [Les meir …](/about/)

![Skjermbilete av Kate med fleire dokument og terminal­emulatoren
oppe](/images/kate-window.png)