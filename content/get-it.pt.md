---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Obter o Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix'es

* Instale o [Kate](https://apps.kde.org/en/kate) ou o [KWrite](https://apps.kde.org/en/kwrite) a partir da [sua distribuição](https://kde.org/distributions/):

{{< get-it-button-discover

button-kate-title="Instalar o KWrite"button-kate-subtitle="através do Discover"button-kate-alt="Instalar o KWrite através do Discover ou de outras lojas de aplicações do AppStream"

button-kwrite-title="Instalar o KWrite"button-kwrite-subtitle="através do Discover"button-kwrite-alt="Instalar o KWrite através do Discover ou de outras lojas de aplicações do AppStream"

>}}

Estes botões só funcionam com o [Discover](https://apps.kde.org/en/discover) e outras lojas de aplicações AppStream, como a [GNOME Software](https://wiki.gnome.org/Apps/Software). Também poderá usar o gestor de pacotes da sua distribuição, como o [Muon](https://apps.kde.org/en/muon) ou o [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

* [O pacote Flatpak do Kate no Flathub](https://flathub.org/apps/details/org.kde.kate)

{{< get-it-button link="https://flathub.org/apps/details/org.kde.kate" img="/images/flathub-badge-i-en.svg" width="200" alt="Download Kate on Flathub" />}}

* [O pacote Snap do Kate no Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-white.svg" width="200" alt="Get Kate on the Snap store" />}}

* [Pacote AppImage da versão 64-bits do Kate](https://binary-factory.kde.org/job/Kate_Release_appimage/) *

* [Pacote AppImage da versão 64-bits diária do Kate](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **

* [Compile-o](/build-it/#linux) a partir do código.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [O Kate na Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.svg" width="200" alt="Get Kate from the Microsoft store" />}}

* [O Kate no Chocolatey](https://chocolatey.org/packages/kate)

* [Instalador do Kate (versão a 64bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *

* [Instalador do Kate (versão diária a 64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **

* [Compile-o](/build-it/#windows) a partir do código.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

* [Instalador da versão do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *

* [Instalador da versão diária do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **

* [Compile-o](/build-it/#mac) a partir do código.

{{< /get-it >}}



{{< get-it-info >}}

**Acerca das versões:** <br>O [Kate](https://apps.kde.org/en/kate) e o [KWrite](https://apps.kde.org/en/kwrite) fazem parte das [Aplicações do KDE](https://apps.kde.org), que são [lançadas tipicamente 3 vezes por ano em massa](https://community.kde.org/Schedules). O [motor de edição de texto](/about-katepart/), os [temas de cores](/themes/) e o [realce de sintaxe](/syntax/) são fornecidos pelas [Plataformas do KDE](https://kde.org/announcements/kde-frameworks-5.0/), que são [actualizadas mensalmente](https://community.kde.org/Schedules/Frameworks). As novas versões vão sendo anunciadas [aqui](https://kde.org/announcements/).

\* Os pacotes **release** contêm a última versão do Kate e das Plataformas do KDE.

\*\* Os pacotes **nightly** são compilados automaticamente todos os dias a partir do código-fonte, pelo que poderão estar instáveis e conter erros ou funcionalidades incompletas. Só são recomendados para fins de testes.