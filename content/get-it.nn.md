---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Last ned Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix-ar

* Installer [Kate](https://apps.kde.org/en/kate) eller [KWrite](https://apps.kde.org/en/kwrite) frå [distribusjonen din](https://kde.org/distributions/):

* [Kate via Discover eller andre AppStream-baserte programbutikkar](appstream://org.kde.kate.desktop)

* [KWrite via Discover eller andre AppStream-baserte programbutikkar](appstream://org.kde.kwrite.desktop)

* [Flatpak-pakke for Kate i Flathub](https://flathub.org/apps/details/org.kde.kate)

* [Snap-pakke for Kate i Snapcraft](https://snapcraft.io/kate)

* [AppImage-pakke for Kate (64 bit)](https://binary-factory.kde.org/job/Kate_Release_appimage/)

* [AppImage-pakke for dagleg oppdatert Kate-versjon (64 bit)](https://binary-factory.kde.org/job/Kate_Nightly_appimage/)

* [Kompiler](/build-it/#linux) frå kjeldekoden.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate i Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Installerings­program for Kate (64 bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Installerings­program for dagleg oppdatert Kate-versjon (64 bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Kompiler](/build-it/#windows) frå kjeldekoden.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Installerings­program for offisiell Kate-versjon](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Installerings­program for dagleg oppdatert Kate-versjon](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Kompiler](/build-it/#mac) frå kjeldekoden.