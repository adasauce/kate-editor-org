# kate-editor.org

This repository contains the full kate-editor.org website.

# Instantiate the page

You can create the static page by running

./update.sh

This will pull the git repo and run hugo with the right settings.

# Live preview

You can start a local hugo powered web server via

./server.sh

The command will print the URL to use for local previewing.

# Update the syntax-highlighting framework update sites

Check out the README.md in the syntax-highlighting.git on invent.kde.org.

There is a build target to generate the needed stuff after a successful compile of the framework.
