#!/usr/bin/python3

import os
import re
import sys
import yaml
import argparse
import gitlab
import hashlib
import datetime

# get all merge requests, hash them by year + month
datetoMerge = {}
repos = { "utilities/kate": "Kate", "frameworks/ktexteditor": "KTextEditor", "frameworks/syntax-highlighting": "KSyntaxHighlighting", "websites/kate-editor-org": "kate-editor.org" }
merged = {}
with gitlab.Gitlab( "https://invent.kde.org", ssl_verify=True ) as gl:
    for path in sorted(repos):
        # get project & merge requests
        project = gl.projects.get(path)
        for request in project.mergerequests.list(state='merged', order_by='updated_at', sort='desc', all=True):
            # merge + creation day, we want to compute how long it took
            mergeDate = datetime.date.fromisoformat(request.merged_at[0:10])
            createDate = datetime.date.fromisoformat(request.created_at[0:10])

            # nice duration
            daysTaken = (mergeDate - createDate).days
            if daysTaken == 0:
                daysTaken = "at creation day"
            elif daysTaken == 1:
                daysTaken = "after one day"
            elif daysTaken > 1:
                daysTaken = "after {} days".format(daysTaken)


            # create nice markdown variant of the request's major infos
            markdownDescription = ""
            markdownDescription += "- [{}]({})\n".format(request.title, request.web_url);
            markdownDescription += "    - Request authored by [{}]({}).\n".format(request.author['name'], request.author['web_url']);
            markdownDescription += "    - Merged in the {} repository {}.\n\n".format(repos[path], daysTaken);

            # store with year -> month -> list of requests pre-formatted for output
            datetoMerge.setdefault(mergeDate.year, {}).setdefault(mergeDate.month, {}).setdefault(mergeDate.day, []).append(markdownDescription)

            # remember how many things got merged for overall stats
            merged.setdefault(path, {}).setdefault(0, 0)
            merged.setdefault(path, {}).setdefault(mergeDate.year, 0)
            merged[path][0] += 1;
            merged[path][mergeDate.year] += 1;

# create merge-requests mark down file
requestsMd = open(sys.path[0] + "/content/merge-requests.md", 'w')
requestsMd.write('''---
title: Merge Requests
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
---

This pages provides an overview about the merge requests we accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

## Overall Accepted Merge Requests

''')

# overall merges
for path in sorted(repos):
    requestsMd.write("- {} patches for [{}](https://invent.kde.org/{}/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)\n".format(merged[path][0], repos[path], path))
requestsMd.write("\n")

# output a nice markdown table for each year & month with all merged requests
for year in sorted(datetoMerge, reverse=True):
    requestsMd.write("\n\n## Accepted Merge Requests of {}\n\n".format(year))

    # merges of the given year
    for path in sorted(repos):
        if year in merged[path]:
            requestsMd.write("- {} patches for [{}](https://invent.kde.org/{}/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)\n".format(merged[path][year], repos[path], path))
    requestsMd.write("\n")

    for month in sorted(datetoMerge[year], reverse=True):
        requestsMd.write("### {}\n\n".format(datetime.date(year, month, 1).strftime('%B %Y')))
        for day in sorted(datetoMerge[year][month], reverse=True):
            for request in datetoMerge[year][month][day]:
                requestsMd.write(request)
